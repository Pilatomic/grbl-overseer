#ifndef APPLICATIONLOGIC_H
#define APPLICATIONLOGIC_H

#include <QObject>
#include <grbl/grblstatuslogic.h>
#include <gcode/gcodejob.h>


class GCodeJobManager;
class GrblBoard;

class ApplicationLogic : public QObject
{
    Q_OBJECT
    Q_PROPERTY(State currentState READ getCurrentState NOTIFY currentStateChanged)
    Q_PROPERTY(bool isIdle READ isIdle NOTIFY isIdleChanged)
    Q_PROPERTY(bool readyToGo READ readyToGo NOTIFY readyToGoChanged)

public:
    enum State {
        State_Idle,
        State_WaitingForSimulationMode, State_Simulation_Running, State_Simulation_Completed,
        State_WaitingForProductionMode, State_Production_Running, State_Production_Completed
    };

    Q_ENUMS(State)

    explicit ApplicationLogic(GCodeJobManager *jobManager = nullptr, GrblBoard *grblBoard = nullptr, QObject *parent = 0);

    State getCurrentState() const
        {return m_currentState;}

    bool isIdle() const
        {return (m_currentState == State_Idle);}

    bool readyToGo() const
        {return m_readyToGo;}

    Q_INVOKABLE QString getRegisteredErrors() const
        {return m_errorsRegistry.join("\n");}

    Q_INVOKABLE int getErrorCount() const
        {return m_errorsRegistry.size();}

signals:
    void currentStateChanged();
    void isIdleChanged();
    void readyToGoChanged();

    void simulationCompleted();
    void productionCompleted();
    void productionError(QString errorMsg);

public slots:
    void startSimulation();
    void startProduction();
    void abort();

    void setJobOriginAtCurrentPosition(int jobIndex);

    void setJobXOriginAtCurrentPosition(int jobIndex);
    void setJobYOriginAtCurrentPosition(int jobIndex);
    void setJobZOriginAtCurrentPosition(int jobIndex);

private slots:
    void onGrblJobCompleted();
    void startIfGrblIsInCorrectMode();
    void updateReadyToGoFlag();
    void onGrblError(QString errorMessage);

private:
    void runNextJob();
    void setState(State state);


    State m_currentState;
    bool m_readyToGo;


    GCodeJobManager *m_jobManager;
    GrblBoard *m_grblBoard;

    QStringList m_errorsRegistry;
};

#endif // APPLICATIONLOGIC_H

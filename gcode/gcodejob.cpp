#include "gcodejob.h"
#include "gcodejobline.h"
#include "gcodeparser.h"

#include <QFile>

#include <QDebug>

#define MAX_LINE_LENGTH         256     //Defined by gcode standard

const QMap<GCodeJob::GCodeJobState, GCodeJob::GCodeJobState> GCodeJob::s_stateTogglingMap =
        QMap<GCodeJob::GCodeJobState, GCodeJob::GCodeJobState> (
        {
            {GCodeJob::Job_Ready,       GCodeJob::Job_Disabled  },
            {GCodeJob::Job_Queued,      GCodeJob::Job_Ready     },
            {GCodeJob::Job_Disabled,    GCodeJob::Job_Ready     },
            {GCodeJob::Job_Completed,   GCodeJob::Job_Ready     },
            {GCodeJob::Job_Running,     GCodeJob::Job_Ready     },
        });


GCodeJob::GCodeJob(QString filePath, QObject *parent):
    QObject(parent),
    m_fileInfo(filePath),
    m_currentState(Job_Empty),
    m_currentLineIndex(0),
    m_totEstMachTime(0),
    m_remEstMachTime(0)
{
    QFile file(m_fileInfo.absoluteFilePath());

    //Attempt to open file as read only
    if (!file.open(QIODevice::ReadOnly))
    {
        setCurrentState(Job_CouldNotOpenFile);
    }
    else
    {
        file.reset();

        quint32 lineNumber = 1;
        GCodeParser parser;

        bool limitsInitialized = false;

        while(!file.atEnd())
        {
            QString lineString = file.readLine(MAX_LINE_LENGTH);

            if(!lineString.endsWith('\n') &&!file.atEnd())
            {
                //We didnt grab a complete line, probably because line length exceeds GCODE standard
                setCurrentState(Job_LineTooLong);
                break;
            }

            //Create corresponding GCodeJobLine
            GCodeJobLine *gcodeLine = new GCodeJobLine(lineString,lineNumber++);
            gcodeLine->update(&parser);
            m_lines.append(gcodeLine);

            if(parser.lastLineContainsCoordOffset())
            {
                setCurrentState(Job_ContainsCoordOffset);
                break;
            }

            //Manage job limits only if current line path is not empty
            const QVector<QVector3D> linePath = parser.getLastLinePathPoints();
            if(!linePath.isEmpty())
            {
                //Limits must be initialized by any job point
                //If not done yet, do it now !
                if(!limitsInitialized)
                {
                    m_limitMin = parser.getCurrentPosition();
                    m_limitMax = parser.getCurrentPosition();
                    limitsInitialized = true;
                }

                //Update limits
                for(QVector<QVector3D>::const_iterator pathPoint = linePath.cbegin();
                    pathPoint != linePath.cend();
                    pathPoint++)
                {
                    if(pathPoint->x() > m_limitMax.x())      m_limitMax.setX(pathPoint->x());
                    else if(pathPoint->x() < m_limitMin.x()) m_limitMin.setX(pathPoint->x());

                    if(pathPoint->y() > m_limitMax.y())      m_limitMax.setY(pathPoint->y());
                    else if(pathPoint->y() < m_limitMin.y()) m_limitMin.setY(pathPoint->y());

                    if(pathPoint->z() > m_limitMax.z())      m_limitMax.setZ(pathPoint->z());
                    else if(pathPoint->z() < m_limitMin.z()) m_limitMin.setZ(pathPoint->z());
                }
            }
        }

        file.close();

        //If no error encountered, update status
        if((m_currentState == Job_Empty) && !m_lines.isEmpty())
        {
            m_endCoordinates = parser.getCurrentPosition();
            setCurrentState(Job_Ready);
        }
    }
}

GCodeJob::~GCodeJob()
{
    qDeleteAll(m_lines);
}

void GCodeJob::updateLineStatus(int line, GCodeJobLine::Status nextStatus)
{
    if(line > 0 && line <= m_lines.size())
    {
        int lineIndex = line -1;
        GCodeJobLine* line = m_lines[lineIndex];
        GCodeJobLine::Status prevStatus = line->getStatus();
        bool lineWasCompleted = line->isCompleted();

        //No status change -> nothing to to
        if(nextStatus == prevStatus)
        {
            return;
        }

        setLineStatusAndNotify(line, nextStatus);

        //If we sent a line after the current line index, update the current line index
        //If we cancelled a line before the current line index, also update it
        if((nextStatus == GCodeJobLine::Line_Queued && lineIndex < m_currentLineIndex)
        || (nextStatus != GCodeJobLine::Line_Queued && lineIndex > m_currentLineIndex))

        {
            setCurrentLine(lineIndex);
        }

        //Line completion changed, we need to update remaining machine time
        if(lineWasCompleted != line->isCompleted())
        {
            if(line->isCompleted())
            {
                m_remEstMachTime -= line->getEstimatedMachineTime();
            }
            else
            {
                m_remEstMachTime += line->getEstimatedMachineTime();
            }

            emit remEstMachTimeChanged(this);
        }
    }
}

const GCodeJobLine *GCodeJob::getLineToRun()
{
    if(m_currentState != Job_Running)
    {
        setCurrentState(Job_Running);
    }

    //locate the first line (starting from m_currentLine) not yet run
    QVector<GCodeJobLine*>::const_iterator nextLineIt;
    for(nextLineIt = m_lines.cbegin() + m_currentLineIndex;
        nextLineIt != m_lines.cend();
        nextLineIt++)
    {
        //Found the next line to run
        if((*nextLineIt)->getStatus() == GCodeJobLine::Line_Queued)
        {
            return (*nextLineIt);
        }
    }

    //Return nullptr if reached end of job
    return nullptr;
}

QString GCodeJob::getName() const
{
    return m_fileInfo.fileName();
}

QString GCodeJob::getPath() const
{
    return m_fileInfo.filePath();
}

QColor GCodeJob::getColor() const
{
    return m_color;
}

void GCodeJob::setColor(QColor color)
{
    if(color != m_color)
    {
        m_color = color;
        emit colorChanged(this, m_color);
    }
}

QVector3D GCodeJob::getLimitMax() const
{
    return m_limitMax;
}

QVector3D GCodeJob::getLimitMin() const
{
    return m_limitMin;
}

quint32 GCodeJob::getLineCount() const
{
    return m_lines.size();
}

quint32 GCodeJob::getCurrentLineNumber() const
{
    return m_currentLineIndex + 1;
}

GCodeJobLine *GCodeJob::getLine(int lineNumber) const
{
    return (lineNumber > 0 && lineNumber <= m_lines.size()) ? m_lines.at(lineNumber - 1) : nullptr;
}

GCodeJob::GCodeJobState GCodeJob::getState() const
{
    return m_currentState;
}

QVector3D GCodeJob::getOrigin() const
{
    return m_originCoords;
}

float GCodeJob::getProgression() const
{
    switch (m_currentState)
    {
    case Job_Ready:
    case Job_Completed:
        return 1.0f;
        break;

    default:
        return ((float)m_currentLineIndex / (float)m_lines.size());
        break;
    }
}

QVector3D GCodeJob::getEndPosition() const
{
    return m_endCoordinates;
}


quint32 GCodeJob::getRemEstMachTime() const
{
    return m_remEstMachTime;
}

quint32 GCodeJob::getTotEstMachTime() const
{
    return m_totEstMachTime;
}

void GCodeJob::markAsCompleted()
{
    setCurrentState(Job_Completed);
    emit progressionChanged(this);

    m_remEstMachTime = 0.0f;
    emit
}

bool GCodeJob::isAtBeginning() const
{
    return m_currentLineIndex == 0;
}

void GCodeJob::flagLine(int line)
{
    if(line > 0 && line <= m_lines.size())
    {
        m_lines[line-1]->setFlagged(true);
    }
}

void GCodeJob::setOrigin(QVector3D origin)
{
    if(m_currentState != Job_Running)
    {
        m_originCoords = origin;
//        qDebug() << "Job origin set to "<<m_originCoords;
        emit originChanged(this,m_originCoords);
    }
}

void GCodeJob::prepare()
{
    setCurrentLine(0);
    foreach(GCodeJobLine* line,m_lines)
    {
        setLineStatusAndNotify(line, GCodeJobLine::Line_Queued);
    }

    if(m_currentState != Job_Disabled)
    {
        setCurrentState(Job_Queued);
    }

    m_remEstMachTime = m_totEstMachTime;

    emit currentLineChanged(this);
    emit progressionChanged(this);
    emit remEstMachTimeChanged(this);
}

void GCodeJob::reset()
{
    setCurrentLine(0);
    foreach(GCodeJobLine* line,m_lines)
    {
        setLineStatusAndNotify(line, GCodeJobLine::Line_Ready);
    }

    if(m_currentState != Job_Disabled)
    {
        setCurrentState(Job_Ready);
    }

    m_remEstMachTime = m_totEstMachTime;

    emit currentLineChanged(this);
    emit progressionChanged(this);
    emit remEstMachTimeChanged(this);
}

void GCodeJob::unflagAllLines()
{
    for(QVector<GCodeJobLine*>::iterator i = m_lines.begin();
        i != m_lines.end();
        i++)
    {
        (*i)->setFlagged(false);
    }
}

bool GCodeJob::setCurrentState(GCodeJobState nextState)
{
    if(m_currentState != nextState)
    {
        m_currentState = nextState;
        emit stateChanged(this);
        return true;
    }

    return false;
}

void GCodeJob::setCurrentLine(qint32 line)
{
    if(line != m_currentLineIndex)
    {
        m_currentLineIndex = line;
        emit currentLineChanged(this);
        emit progressionChanged(this);
    }
}

void GCodeJob::setLineStatusAndNotify(GCodeJobLine *line, GCodeJobLine::Status status)
{
    if(line->getStatus() != status)
    {
        line->setStatus(status);
        emit lineStatusUpdated(this, line->getLineNumber(), line->getStatus());
    }
}


void GCodeJob::updateEstimatedMachineTimes(QVector3D machineSeekRatePerAxis)
{
    quint32 totalEstimatedMachineTime = 0;
    quint32 remainingEstimatedMachineTime = 0;

    foreach(GCodeJobLine *line,m_lines)
    {
        line->updateEstimatedMachineTime(machineSeekRatePerAxis);
        quint32 lineEstimatedMachineTime = line->getEstimatedMachineTime();

        totalEstimatedMachineTime += lineEstimatedMachineTime ;

        if(!line->isCompleted())
            remainingEstimatedMachineTime += lineEstimatedMachineTime;
    }

    if(totalEstimatedMachineTime != m_totEstMachTime)
    {
        m_totEstMachTime = totalEstimatedMachineTime;
        emit totEstMachTimeChanged(this);
    }

    if(remainingEstimatedMachineTime != m_remEstMachTime)
    {
        m_remEstMachTime = remainingEstimatedMachineTime;
        emit remEstMachTimeChanged(this);
    }
}


void GCodeJob::toggleState()
{
    if(s_stateTogglingMap.contains(m_currentState))
    {
        GCodeJobState nextState = s_stateTogglingMap.value(m_currentState);

        //If we changed to running state, reset !
        if(setCurrentState(nextState))
        {
            reset();
        }
    }
}

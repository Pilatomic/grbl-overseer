#ifndef GCODEJOB_H
#define GCODEJOB_H

#include "gcodejobline.h"

#include <QObject>
#include <QVector3D>
#include <QVector>
#include <QColor>
#include <QPointer>
#include <QFileInfo>


class GCodeJob : public QObject
{
    Q_OBJECT

public:
    enum GCodeJobState {Job_Empty, Job_CouldNotOpenFile, Job_LineTooLong, Job_ContainsCoordOffset,
                         Job_Ready, Job_Queued, Job_Disabled, Job_Running, Job_Completed};

    Q_ENUMS(GCodeJobState)

    explicit GCodeJob(QString filePath = QString(), QObject *parent = 0);
    ~GCodeJob();

    //Common accessors
    QString getName() const;
    QString getPath() const;
    quint32 getLineCount() const;
    quint32 getCurrentLineNumber() const;
    GCodeJobLine* getLine(int lineNumber) const;
    GCodeJobState getState() const;
    QVector3D getOrigin() const;
    float getProgression() const;
    QVector3D getEndPosition() const;

    QColor getColor() const;
    void setColor(QColor color);

    QVector3D getLimitMin() const;
    QVector3D getLimitMax() const;

    void markAsCompleted();

    //Those are used by GrblBoard
    void updateLineStatus(int line, GCodeJobLine::Status nextStatus);
    const GCodeJobLine *getLineToRun();
    bool isAtBeginning() const;
    void flagLine(int line);

    quint32 getRemEstMachTime() const;
    quint32 getTotEstMachTime() const;
    void updateEstimatedMachineTimes(QVector3D machineSeekRatePerAxis = QVector3D(0.0f,0.0f,0.0f));

public slots:
    void toggleState();
    void setOrigin(QVector3D origin);
    void prepare();
    void reset();
    void unflagAllLines();

signals:
    void stateChanged(GCodeJob* job);
    void hasReset(GCodeJob* job);       //Only used to speed up visualizer by preventing 1 event per line state change on reset
    void originChanged(GCodeJob* job, QVector3D position);
    void lineStatusUpdated(GCodeJob* job, int line, GCodeJobLine::Status lineStatus);
    void remEstMachTimeChanged(GCodeJob* job);
    void totEstMachTimeChanged(GCodeJob* job);
    void currentLineChanged(GCodeJob* job);
    void progressionChanged(GCodeJob* job);
    void colorChanged(GCodeJob* job, QColor color);

private:
    static const QMap<GCodeJob::GCodeJobState, GCodeJob::GCodeJobState> s_stateTogglingMap;
    bool setCurrentState(GCodeJobState nextState);
    void setCurrentLine(qint32 line);

    void setLineStatusAndNotify(GCodeJobLine* line, GCodeJobLine::Status status);

    QFileInfo m_fileInfo;
    QColor m_color;

    //Offset to set at the beginning of the job
    QVector3D m_originCoords;

    //Those must be added to the offset to get absolute coordinates
    QVector3D m_endCoordinates;
    QVector3D m_limitMin;   //Minimum value for each job coordinate
    QVector3D m_limitMax;   //Maximum value for each job coordinate

//    //Was meant to be used to specify origin relative to another job. Not implemented
//    QPointer<GCodeJob> m_jobRelativeOrigin;

    QVector<GCodeJobLine*> m_lines;
    GCodeJobState m_currentState;
    qint32 m_currentLineIndex;

    quint32 m_totEstMachTime;
    quint32 m_remEstMachTime;
};

#endif // GCODEJOB_H

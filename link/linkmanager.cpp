#include "linkmanager.h"

#ifdef Q_OS_ANDROID
#include <android/androidusbbridgeserialport.h>
#else
#include <QSerialPort>
#include <QSerialPortInfo>
#endif

#include <QDebug>

#define RESCAN_INTERVAL         2000
#define SCAN_SHORT_INTERVAL     100
#define DEFAULT_BAUDRATE        115200

const QStringList LinkManager::s_standardBaudratesList = QStringList({QString::number(DEFAULT_BAUDRATE), "9600"});

LinkManager::LinkManager(QObject *parent) :
    QObject(parent),
    m_baudrateList(s_standardBaudratesList),
    m_linkDevice(nullptr)
{
    m_scanTimer = new QTimer(this);
    m_scanTimer->setSingleShot(true);
    m_scanTimer->start(SCAN_SHORT_INTERVAL);
    connect(m_scanTimer, &QTimer::timeout, this, &LinkManager::rescanAvailablePorts);
}

bool LinkManager::isLinkOpen()
{
    return m_linkDevice && m_linkDevice->isOpen();
}


void LinkManager::closeLink()
{
    if(m_linkDevice)
    {
        m_linkDevice->close();
        emit linkOpenChanged(false);
        emit linkDeviceChanged(nullptr);
        delete m_linkDevice;
        m_linkDevice = nullptr;
        m_scanTimer->start(SCAN_SHORT_INTERVAL);
    }
}

void LinkManager::openLink(QString portName, QString baudRate)
{
    closeLink();

#ifdef Q_OS_ANDROID
    AndroidUsbBridgeSerialPort* serialPort = new AndroidUsbBridgeSerialPort();
#else // NOT Q_OS_ANDROID
    QSerialPort* serialPort = new QSerialPort(this);
#endif

    serialPort->setPortName(portName);
    if(!serialPort->open(QIODevice::ReadWrite))
    {
        delete serialPort;
        return;
    }

    bool baudIntOk = false;
    qint32 baudInt = baudRate.toInt(&baudIntOk);
    if(!baudIntOk) baudInt = DEFAULT_BAUDRATE;

#ifdef Q_OS_ANDROID
    serialPort->setParameters(baudInt,8,1,0);
#else // NOT Q_OS_ANDROID
    serialPort->setBaudRate(baudInt);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 8, 0))
    //Disconnect port on error, signal available fot Qt version >= 5.8
    connect(serialPort, &QSerialPort::errorOccurred, this, &LinkManager::closeLink);
#endif

    m_linkDevice = serialPort;

    m_scanTimer->stop();
    m_availablePortList = QStringList(portName);
    m_baudrateList = QStringList(QString::number(baudInt));

    emit linkDeviceChanged(m_linkDevice);
    emit linkOpenChanged(true);
    emit portListUpdated();
    emit baudRateListUpdated();
}

void LinkManager::rescanAvailablePorts()
{
#ifdef Q_OS_ANDROID

    QStringList updatedPortList = AndroidUsbBridgeSerialPort::scanForAvailablePorts();

#else // NOT Q_OS_ANDROID

    //Build a string list containing port names
    QList<QSerialPortInfo> portInfoList = QSerialPortInfo::availablePorts();
    QStringList updatedPortList;
    for(QList<QSerialPortInfo>::const_iterator i = portInfoList.cbegin();
        i < portInfoList.cend();
        i++)
    {
        updatedPortList.append(i->portName());
    }

#endif

    if(updatedPortList != m_availablePortList)
    {
//        qDebug() << "Available ports changed :" << updatedPortList;
        m_availablePortList = updatedPortList;
        emit portListUpdated();
    }

    m_scanTimer->start(RESCAN_INTERVAL);

    if(m_baudrateList != s_standardBaudratesList) m_baudrateList = s_standardBaudratesList;
    emit baudRateListUpdated();
}

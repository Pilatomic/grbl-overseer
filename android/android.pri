QT -= serialport
QT += androidextras

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-sources

OTHER_FILES = \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/CdcAcmSerialDriver.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/CommonUsbSerialDriver.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/Cp2102SerialDriver.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/FtdiSerialDriver.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/ProlificSerialDriver.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/UsbId.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/UsbSerialDriver.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/UsbSerialProber.java \
        $$PWD/android-sources/src/com/hoho/android/usbserial/driver/UsbSerialRuntimeException.java \
        $$PWD/android-sources/src/org/qtproject/qtserialport/android/usbdevice/UsbDeviceJNI.java \
        $$PWD/android-sources/src/org/qtproject/qtserialport/android/usbdevice/UsbIoManager.java \
        $$PWD/android-sources/res/xml/device_filter.xml \
        $$PWD/android-sources/AndroidManifest.xml

HEADERS += $$PWD/androidusbbridgeserialport.h \
    $$PWD/keepawakehelper.h \
    $$PWD/ringbuffer.h

SOURCES += $$PWD/androidusbbridgeserialport.cpp \
    $$PWD/keepawakehelper.cpp \
    $$PWD/ringbuffer.cpp

#include "androidusbbridgeserialport.h"

#include <QtAndroidExtras/QtAndroidExtras>
#include <QtAndroidExtras/QAndroidJniObject>

#define BAD_PORT 0

static char V_jniClassName[] {"org/qtproject/qtserialport/android/usbdevice/UsbDeviceJNI"};



static void jniDeviceHasDisconnected(JNIEnv *envA, jobject thizA, jint userDataA)
{
    Q_UNUSED(envA);
    Q_UNUSED(thizA);

    if (userDataA != 0)
        ((AndroidUsbBridgeSerialPort *)userDataA)->close();
}



static void jniDeviceNewData(JNIEnv *envA, jobject thizA, jint userDataA, jbyteArray dataA)
{
    Q_UNUSED(thizA);

    if (userDataA != 0)
    {
        jbyte *bytesL = envA->GetByteArrayElements(dataA, NULL);
        jsize lenL = envA->GetArrayLength(dataA);
        ((AndroidUsbBridgeSerialPort *)userDataA)->newDataArrived((char *)bytesL, lenL);
        envA->ReleaseByteArrayElements(dataA, bytesL, JNI_ABORT);
    }
}



static void jniDeviceException(JNIEnv *envA, jobject thizA, jint userDataA, jstring messageA)
{
    Q_UNUSED(thizA);

    if(userDataA != 0)
    {
        const char *stringL = envA->GetStringUTFChars(messageA, NULL);
        QString strL = QString::fromUtf8(stringL);
        envA->ReleaseStringUTFChars(messageA, stringL);
        if(envA->ExceptionCheck())
            envA->ExceptionClear();
        //((AndroidUsbBridgeSerialPort *)userDataA)->exceptionArrived(strL);
    }
}


AndroidUsbBridgeSerialPort::AndroidUsbBridgeSerialPort() :
    QIODevice(),
    isReadStopped(true),
    descriptor(-1),
    //hasRegisteredFunctions(false),
    pendingBytesWritten(0),
    readBufferMaxSize(0),
    internalWriteTimeoutMsec(0)
{

}

void AndroidUsbBridgeSerialPort::setPortName(QString portName)
{
    systemLocation = portName;
}

void AndroidUsbBridgeSerialPort::setParameters(int baudRateA, int dataBitsA, int stopBitsA, int parityA)
{
    if (deviceId == BAD_PORT)
    {
        return;
    }

    QAndroidJniObject::callStaticMethod<jboolean>(V_jniClassName,
                                                 "setParameters",
                                                 "(IIIII)Z",
                                                 deviceId,
                                                 baudRateA,
                                                 dataBitsA,
                                                 stopBitsA,
                                                  parityA);
}

QStringList AndroidUsbBridgeSerialPort::scanForAvailablePorts()
{
    QStringList updatedPortList;

    QAndroidJniObject resultL = QAndroidJniObject::callStaticObjectMethod(V_jniClassName,
                                                                          "availableDevicesInfo",
                                                                          "()[Ljava/lang/String;");
    if (resultL.isValid())
    {
        QAndroidJniEnvironment envL;
        jobjectArray objArrayL = resultL.object<jobjectArray>();
        int countL = envL->GetArrayLength(objArrayL);

        for (int iL=0; iL<countL; iL++)
        {
            jstring stringL = (jstring)(envL->GetObjectArrayElement(objArrayL, iL));
            const char *rawStringL = envL->GetStringUTFChars(stringL, 0);
            QStringList strListL = QString::fromUtf8(rawStringL).split(QStringLiteral(":"));
            envL->ReleaseStringUTFChars(stringL, rawStringL);

            updatedPortList.append(strListL[0]);
        }
    }
    return updatedPortList;
}

void AndroidUsbBridgeSerialPort::stopReadThread()
{
    if (isReadStopped)
        return;

    QAndroidJniObject::callStaticMethod<void>(V_jniClassName,
                                              "stopIoManager",
                                              "(I)V",
                                              deviceId);
    isReadStopped = true;
}



void AndroidUsbBridgeSerialPort::startReadThread()
{
    if (!isReadStopped)
        return;

    QAndroidJniObject::callStaticMethod<void>(V_jniClassName,
                                              "startIoManager",
                                              "(I)V",
                                              deviceId);
    isReadStopped = false;
}

bool AndroidUsbBridgeSerialPort::writeDataOneShot()
{
    pendingBytesWritten = -1;

    while (!writeBuffer.isEmpty())
    {
        pendingBytesWritten = writeToPort(writeBuffer.readPointer(), writeBuffer.nextDataBlockSize());

        /*if (pendingBytesWritten <= 0)
        {
            QSerialPort::SerialPortError errorL = decodeSystemError();
            if (errorL != QSerialPort::ResourceError)
                errorL = QSerialPort::WriteError;
            q->setError(errorL);
            return false;
        }*/

        writeBuffer.free(pendingBytesWritten);

        emit bytesWritten(pendingBytesWritten);
    }

    return (pendingBytesWritten < 0)? false: true;
}

qint64 AndroidUsbBridgeSerialPort::writeToPort(const char *data, qint64 maxSize)
{
    if (deviceId == BAD_PORT)
    {
//        q_ptr->setError(QSerialPort::NotOpenError);
        return 0;
    }

    QAndroidJniEnvironment envL;
    jbyteArray jarrayL = envL->NewByteArray(maxSize);
    envL->SetByteArrayRegion(jarrayL, 0, maxSize, (jbyte *)data);
    int resultL = QAndroidJniObject::callStaticMethod<jint>(V_jniClassName,
                                                            "write",
                                                            "(I[BI)I",
                                                            deviceId,
                                                            jarrayL,
                                                            internalWriteTimeoutMsec);

    if (envL->ExceptionCheck())
    {
        envL->ExceptionClear();
        //q_ptr->setErrorString(QStringLiteral("Writing to the device threw an exception"));
        envL->DeleteLocalRef(jarrayL);
        return 0;
    }

    envL->DeleteLocalRef(jarrayL);

    return resultL;
}


void AndroidUsbBridgeSerialPort::newDataArrived(char *bytesA, int lengthA)
{
    int bytesToReadL = lengthA;

    // Always buffered, read data from the port into the read buffer
    if (readBufferMaxSize && (bytesToReadL > (readBufferMaxSize - readBuffer.size()))) {
        bytesToReadL = readBufferMaxSize - readBuffer.size();
        if (bytesToReadL <= 0) {
            // Buffer is full. User must read data from the buffer
            // before we can read more from the port.
            stopReadThread();
            return;
        }
    }

    char *ptr = readBuffer.reserve(bytesToReadL);
    memcpy(ptr, bytesA, bytesToReadL);

    emit readyRead();
}

bool AndroidUsbBridgeSerialPort::open(QIODevice::OpenMode)
{
    QAndroidJniObject jnameL = QAndroidJniObject::fromString(systemLocation);
    deviceId = QAndroidJniObject::callStaticMethod<jint>(V_jniClassName,
                                                         "open",
                                                         "(Ljava/lang/String;I)I",
                                                         jnameL.object<jstring>(),
                                                         (jint)this);

    isReadStopped = false;

    qDebug() << "Device Id : "<< deviceId;

    if (deviceId == BAD_PORT)
    {
        qDebug() <<"BAD PORT";
        return false;
    }

    descriptor = QAndroidJniObject::callStaticMethod<jint>(V_jniClassName,
                                                           "getDeviceHandle",
                                                           "(I)I",
                                                           deviceId);

    qDebug() << "Descriptor" << descriptor;

    /*if (!hasRegisteredFunctions)
    {
        //  REGISTER THE C++ FUNCTION WITH JNI
        QAndroidJniEnvironment envL;
        JNINativeMethod methodsL[] {{"nativeDeviceHasDisconnected", "(I)V", reinterpret_cast<void *>(jniDeviceHasDisconnected)},
            {"nativeDeviceNewData", "(I[B)V", reinterpret_cast<void *>(jniDeviceNewData)},
            {"nativeDeviceException", "(ILjava/lang/String;)V", reinterpret_cast<void *>(jniDeviceException)}};
        //QAndroidJniObject javaClassL(V_jniClassName);

#warning debug attempt
        if (envL->ExceptionCheck())
            envL->ExceptionClear();

        //qDebug() << "Before GetObjectClass" << (javaClassL.isValid() ? "JavaClass valid" : "JavaClass invalid");
        //jclass objectClassL = envL->GetObjectClass(javaClassL.object<jobject>());
        //qDebug() << "After GetobjectClass";

        jclass objectClassL = envL->FindClass(V_jniClassName);
        qDebug()<<"Class"<<objectClassL;

        jint valL = envL->RegisterNatives(objectClassL, methodsL, sizeof(methodsL) / sizeof(methodsL[0]));
        envL->DeleteLocalRef(objectClassL);
        hasRegisteredFunctions = true;

        if (envL->ExceptionCheck())
            envL->ExceptionClear();

        if(valL < 0)
            return false;
    }*/

    QIODevice::open(QIODevice::ReadWrite);

    return true;
}

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNINativeMethod methodsL[] {{"nativeDeviceHasDisconnected", "(I)V", reinterpret_cast<void *>(jniDeviceHasDisconnected)},
        {"nativeDeviceNewData", "(I[B)V", reinterpret_cast<void *>(jniDeviceNewData)},
        {"nativeDeviceException", "(ILjava/lang/String;)V", reinterpret_cast<void *>(jniDeviceException)}};

    jclass javaClass;
    JNIEnv* env;

    if(vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
    {
        return JNI_ERR;
    }

    javaClass = env->FindClass(V_jniClassName);
    if(!javaClass)
    {
        return JNI_ERR;
    }

    if(env->RegisterNatives(javaClass, methodsL, sizeof(methodsL) / sizeof(methodsL[0])) < 0)
    {
        return JNI_ERR;
    }

    return JNI_VERSION_1_6;
}

void AndroidUsbBridgeSerialPort::close()
{
    if (deviceId == BAD_PORT)
        return;

    jboolean resultL = QAndroidJniObject::callStaticMethod<jboolean>(V_jniClassName,
                                                                     "close",
                                                                     "(I)Z",
                                                                     deviceId);
    descriptor = -1;
    pendingBytesWritten = 0;
    deviceId = BAD_PORT;

    QIODevice::close();

//    if (!resultL)
//        q_ptr->setErrorString(QStringLiteral("Closing device failed"));
}

qint64 AndroidUsbBridgeSerialPort::readData(char *data, qint64 maxlen)
{
    qint64 retL = readBuffer.read(data, maxlen);
    startReadThread();
    return retL;
}

qint64 AndroidUsbBridgeSerialPort::writeData(const char *data, qint64 len)
{
    ::memcpy(writeBuffer.reserve(len), data, len);
    if (!writeBuffer.isEmpty())
        writeDataOneShot();

    qDebug() << "Wrote" << len << "data to output buffer";
    return len;
}

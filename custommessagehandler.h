#ifndef CUSTOMMESSAGEHANDLER_H
#define CUSTOMMESSAGEHANDLER_H

#include <QObject>

class CustomMessageHandler : public QObject
{
    Q_OBJECT
private:
  // Constructeur/destructeur
  explicit CustomMessageHandler(QObject *parent = 0);


  void printMessage(QString message);
  static CustomMessageHandler *s_handler;

public:
    static CustomMessageHandler *getHandlerPtr ();
    static void MessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

signals:
    void newMessage(QString message);
};

#endif // CUSTOMMESSAGEHANDLER_H

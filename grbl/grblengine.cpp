#include "grblengine.h"
#include "grbldefinitions.h"
#include "activities/grblabstractactivity.h"

#include <QIODevice>
#include <QTimer>
#include <QDebug>

#define MANUAL_RESET_DELAY  5000

const QMap<GrblEngine::RealTimeCommand, char> GrblEngine::s_rtCmdCharMap =
        QMap<GrblEngine::RealTimeCommand, char>(
        {
            {GrblEngine::RtCmd_FeedHold             , CMD_FEEDHOLD_CHAR         },
            {GrblEngine::RtCmd_Resume               , CMD_RESUME_CHAR           },
            {GrblEngine::RtCmd_StatusRequest        , CMD_STATUS_REQ_CHAR       },
            {GrblEngine::RtCmd_SoftReset            , CMD_SOFT_RESET_CHAR       },
            {GrblEngine::RtCmd_SafetyDoor           , CMD_SAFETY_DOOR_CHAR      },
            {GrblEngine::RtCmd_JogCancel            , CMD_JOG_CANCEL            },
            {GrblEngine::RtCmd_FeedRateNormal       , CMD_FEEDRATE_NORMAL       },
            {GrblEngine::RtCmd_FeedRateInc          , CMD_FEEDRATE_INC          },
            {GrblEngine::RtCmd_FeedRateDec          , CMD_FEEDRATE_DEC          },
            {GrblEngine::RtCmd_FeedRateIncFine      , CMD_FEEDRATE_INC_FINE     },
            {GrblEngine::RtCmd_FeedRateDecFine      , CMD_FEEDRATE_DEC_FINE     },
            {GrblEngine::RtCmd_RapidRateNormal      , CMD_RAPIDRATE_NORMAL      },
            {GrblEngine::RtCmd_RapidRateSlow        , CMD_RAPIDRATE_HALF        },
            {GrblEngine::RtCmd_RapidRateSlower      , CMD_RAPIDRATE_QUARTER     },
            {GrblEngine::RtCmd_SpindleNormal        , CMD_SPINDLE_NORMAL        },
            {GrblEngine::RtCmd_SpindleInc           , CMD_SPINDLE_INC           },
            {GrblEngine::RtCmd_SpindleDec           , CMD_SPINDLE_DEC           },
            {GrblEngine::RtCmd_SpindleIncFine       , CMD_SPINDLE_INC_FINE      },
            {GrblEngine::RtCmd_SpindleDecFine       , CMD_SPINDLE_DEC_FINE      },
            {GrblEngine::RtCmd_ToggleSpindleStop    , CMD_TOGGLE_SPINDLE_STOP   },
            {GrblEngine::RtCmd_ToggleFloodCoolant   , CMD_TOGGLE_FLOOD_COOL     },
            {GrblEngine::RtCmd_ToggleMistCoolant    , CMD_TOGGLE_MIST_COOL      },
        }
        );

GrblEngine::GrblEngine(QObject *parent) :
    QObject(parent),
    m_linkDevice(nullptr),
    m_grblCharBufferDepth(GRBL_DEFAULT_CHAR_BUFFER_SIZE),
    m_resetInProgress(false)
{
    m_manualResetTimer = new QTimer(this);
    m_manualResetTimer->setInterval(MANUAL_RESET_DELAY);

    connect(m_manualResetTimer, &QTimer::timeout,
            [=]()
    {
        pushRealTimeCommand(RtCmd_SoftReset);
    });
}


void GrblEngine::setLinkDevice(QIODevice *device)
{
    //Cancel any signal / slot connection with previous link device
    if(m_linkDevice)
    {
        m_linkDevice->disconnect(this);
    }

    m_linkDevice = device;

    //If the device is now valid, create signal / slot connection
    if(m_linkDevice)
    {
        connect(m_linkDevice, &QIODevice::readyRead,
                this, &GrblEngine::parseRxBuffer);

        m_resetInProgress = true;

        //Some board will reset automatically when serial link is opened while some won't
        //if we do not received welcome msg before this timer ends, send a soft reset command
        m_manualResetTimer->start();
    }

    emit linkDeviceChanged();
}


void GrblEngine::setBoardCharBufferDepth(int depth)
{
    //Set buffer depth, clamped to 0
    m_grblCharBufferDepth = qMax(0, depth);
}

bool GrblEngine::isOpened()
{
    return m_linkDevice && m_linkDevice->isOpen();
}

bool GrblEngine::isAvailable()
{
    return  isOpened() && !m_resetInProgress;
}

bool GrblEngine::isIdle()
{
    return isAvailable() && m_currentActivity.isNull() ;
}

GrblAbstractActivity *GrblEngine::getCurrentActivity()
{
    return m_currentActivity;
}

bool GrblEngine::isBlockingInstructionInBuffer(void)
{
    for(int i = 0 ; i < m_grblCharBuffer.size() ; i++)
    {
        if(m_grblCharBuffer.at(i)->isBlocking()) return true;
    }
    return false;
}

int GrblEngine::getAvailableSpaceInCharBuffer(void)
{
    int freeSizeInBoardRxBuffer = m_grblCharBufferDepth;

    for(int i = 0 ; i < m_grblCharBuffer.size() ; i++)
    {
        freeSizeInBoardRxBuffer -= m_grblCharBuffer.at(i)->getLength();
    }
    return freeSizeInBoardRxBuffer;
}


void GrblEngine::abortAllInstructions()
{
    //Mark all instruction as aborted and remove them from grbl char buffer
    while(!m_grblCharBuffer.isEmpty())
    {
        GrblInstructionPointer abortedinstPtr = m_grblCharBuffer.takeFirst();
        abortedinstPtr->setStatus(GrblInstruction::Status_Aborted);
        emit instructionCompleted(abortedinstPtr);
    }
}

void GrblEngine::abortJogInstructions()
{
    //Mark jog instructions as aborted and remove them from char buffer
    int i = 0;
    while( i < m_grblCharBuffer.size())
    {
        if(m_grblCharBuffer.at(i)->isJog())
        {
            GrblInstructionPointer abortedinstPtr = m_grblCharBuffer.takeAt(i);
            abortedinstPtr->setStatus(GrblInstruction::Status_Aborted);
            emit instructionCompleted(abortedinstPtr);
        }
        else
        {
            i++;
        }
    }
}

bool GrblEngine::executeActivity(GrblAbstractActivity *activityToExecute)
{
    //First check that activityToExecute is valid, and there is no current activity
    if(!isIdle() || activityToExecute == nullptr)
    {
        return false;
    }

    m_currentActivity = activityToExecute;

    connect(m_currentActivity, &GrblAbstractActivity::rtcmdToEngine,
            this, &GrblEngine::pushRealTimeCommand);

    emit currentActivityChanged(m_currentActivity);

    //Schedule sending next instruction from event loop
    QTimer::singleShot(0, this, &GrblEngine::pushActivityInstructions);

    return true;
}

void GrblEngine::pushActivityInstructions()
{
    while(1)
    {
        if(m_currentActivity.isNull()
                || m_resetInProgress
                || isBlockingInstructionInBuffer())
        {
            return;
        }

        //Retrieve instruction from activity
        GrblInstructionPointer instToPushPtr = m_currentActivity->getNextInstruction();

        //Abort if instruction is not valid
        if(instToPushPtr.isNull())
        {
            //Only if character buffer is empty, it means activity is completed !
            if(m_grblCharBuffer.isEmpty())
            {
                //Notify it of completion after removing it from current activity pointer
                GrblAbstractActivity* completedActivity = m_currentActivity.data();
                m_currentActivity.clear();

                completedActivity->onCompleted();
                completedActivity->deleteLater();

                emit currentActivityChanged(nullptr);
            }

            return;
        }

        // Abort if instruction does not fit in character buffer
        // If it does not fit but buffer is empty, still try to send it !
        if(instToPushPtr->getLength() >= getAvailableSpaceInCharBuffer() &&
                !m_grblCharBuffer.isEmpty())
        {
            return;
        }

        //Attempt to write instruction to link device
        if(m_linkDevice->write(instToPushPtr->getBytes()) > 0)
        {
            instToPushPtr->setStatus(GrblInstruction::Status_Sent);         // Mark instruction as sent ...
            m_grblCharBuffer.append(instToPushPtr);                         // ... it is now in Gbl char buffer
            m_currentActivity->onInstructionSent(instToPushPtr);            // Notify activity
            emit instructionSent(instToPushPtr);                            // Notify others
        }
    }
}

bool GrblEngine::pushRealTimeCommand(GrblEngine::RealTimeCommand command)
{
    char charToPush = s_rtCmdCharMap.value(command, 0x00);

    if(!isOpened()
    || charToPush == 0x00
    || (m_linkDevice->write(&charToPush,1) <= 0))
    {
        return false;
    }

    //Force to process all received data while instructions are still in buffer
    parseRxBuffer();

    //Some commands will have effects on instructions in buffer
    switch(command)
    {
    case RtCmd_JogCancel:
        abortJogInstructions();
        m_currentActivity->deleteLater();
        m_currentActivity.clear();
        emit currentActivityChanged(nullptr);
        break;

    case RtCmd_SoftReset:
        m_resetInProgress = true;
        break;

    default:
        break;
    }

    return true;
}

void GrblEngine::parseRxBuffer()
{
    //Add received data to
    m_rxBuffer.append(m_linkDevice->readAll());

    //Parse buffer
    while(m_rxBuffer.contains(LINE_SEPARATOR_STRING)){
        //Locate next line separator, marking end of the current line
        int lineSeparatorIndex = m_rxBuffer.indexOf(LINE_SEPARATOR_STRING,0);

        //We got a complete line to parse, extract it from buffer
        QString line = QString::fromLatin1(m_rxBuffer.left(lineSeparatorIndex));
        m_rxBuffer.remove(0,lineSeparatorIndex+LINE_SEPARATOR_LENGTH);

        //No need to process empty line
        if(line.isEmpty())  continue;

        GrblMessage message(line);

        //Board reset
        if(message.getType() == GrblMessage::Type_Welcome)
        {
            abortAllInstructions();
            if(m_currentActivity)
            {
                delete m_currentActivity.data();
                m_currentActivity.clear();
                emit currentActivityChanged(nullptr);
            }
            m_manualResetTimer->stop();
            m_resetInProgress = false;
        }

        //If this message update instruction status, BUT resetting is in progress ...
        //... no point in actually processing message, since instruction is already aborted
        else if(message.isInstructionResponse() && m_resetInProgress)
        {
            continue;
        }

        //At this point, we process messages related to an instruction
        else if(!m_grblCharBuffer.isEmpty())
        {
            GrblInstructionPointer relatedInstructionPtr = m_grblCharBuffer.first();

            //For Ok and Error message, we must process the related instruction
            if(message.isInstructionResponse())
            {
                m_grblCharBuffer.removeFirst();

                switch(message.getType())
                {
                case GrblMessage::Type_Ok:
                    relatedInstructionPtr->setStatus(GrblInstruction::Status_Ok);
                    break;

                case GrblMessage::Type_Error:
                    relatedInstructionPtr->setStatus(GrblInstruction::Status_Error);
                    break;

                default:
                    break;
                }

                //Handle activity-related actions
                if(m_currentActivity)
                {
                    //Notify activity of instruction completion
                    m_currentActivity->onInstructionCompleted(relatedInstructionPtr);

                    //Schedule sending next instruction from event loop
                    QTimer::singleShot(0, this, &GrblEngine::pushActivityInstructions);
                }

                //Then notify others
                emit instructionCompleted(relatedInstructionPtr);
            }
            message.setRelatedInstruction(relatedInstructionPtr);
        }

        //Emit signal containing received response
        emitMessageReceivedSignal(message);
    }
}

void GrblEngine::emitMessageReceivedSignal(const GrblMessage &message)
{
    switch(message.getType())
    {
    case GrblMessage::Type_Ok:
        emit msgReceivedTypeOk(message);
        break;

    case GrblMessage::Type_Error:
        emit msgReceivedTypeError(message);
        break;

    case GrblMessage::Type_Alarm:
        emit msgReceivedTypeAlarm(message);
        break;

    case GrblMessage::Type_Status:
        emit msgReceivedTypeStatus(message);
        break;

    case GrblMessage::Type_Feedback:
        emit msgReceivedTypeFeedback(message);
        break;

    case GrblMessage::Type_Welcome:
        emit msgReceivedTypeWelcome(message);
        break;

    case GrblMessage::Type_Parameter:
        emit msgReceivedTypeParameter(message);
        break;

    case GrblMessage::Type_Unknown:
        emit msgReceivedTypeUnknown(message);
        break;

    default:
        break;
    }
}

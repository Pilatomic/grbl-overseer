#include "grblboard.h"
#include "grbldefinitions.h"
#include "grblconfiglogic.h"
#include "grblstringregister.h"
#include "grblengine.h"

#include <QRegularExpression>
#include <QtMath>

#include <QDebug>

#define PARSING_ERROR       -1
#define PARSING_NO_CHANGE   -2
#define ALL_PROPERTIES      -3

const QRegularExpression GrblConfigLogic::s_paramRegExp =
        QRegularExpression("^\\$(?<key>[\\d]{1,3})=(?<value>[\\d][\\d|\\.]*)(\\s\\((.+)\\))?$",
                           QRegularExpression::OptimizeOnFirstUsageOption);

GrblConfigLogic::GrblConfigLogic(GrblEngine *engine,GrblBoard *parentBoard) :
    GrblConfigIface(parentBoard)
{
    connect(engine, &GrblEngine::instructionSent,
            this, &GrblConfigLogic::onPotentiallyConfigInstructionSent);

    connect(engine, &GrblEngine::instructionCompleted,
            this, &GrblConfigLogic::onPotentiallyConfigInstructionCompleted);

    //Receive message type parameter
    connect(engine, &GrblEngine::msgReceivedTypeParameter,
            this, &GrblConfigLogic::onParameterMessageReceived);

    //Clear config on board restart (welcome message) and device changed
    connect(engine, &GrblEngine::msgReceivedTypeWelcome,
            this, &GrblConfigLogic::clear);

    connect(engine, &GrblEngine::linkDeviceChanged,
            this, &GrblConfigLogic::clear);
}

int GrblConfigLogic::parseConfigurationLine(QString line){
    const QRegularExpressionMatch match = s_paramRegExp.match(line);

    if(!match.hasMatch())
    {
        return PARSING_ERROR;
    }

    //Now extract infos
    bool intConversionOk, floatConversionOk;
    const int key = match.captured("key").toInt(&intConversionOk);
    const double value = match.captured("value").toDouble(&floatConversionOk);
    if(!intConversionOk || !floatConversionOk)
    {
        return PARSING_ERROR; //Parsing issue encountered !
    }

    const bool isKeyAlreadyKnow = m_parametersMap.contains(key);
    if(isKeyAlreadyKnow && (m_parametersMap.value(key) == value))
    {
        return PARSING_NO_CHANGE; //No need to modify anything
    }


    //Update model and notidy views
    const int keyIndex = getIndexFromKey(key);
    if(isKeyAlreadyKnow)
    {
        m_parametersMap.insert(key,value);
        QModelIndex changedIndex = index(keyIndex,0);
        emit dataChanged(changedIndex, changedIndex,
                         QVector<int>({ValueRole}));
    }
    else
    {
        beginInsertRows(QModelIndex(),keyIndex,keyIndex);
        m_parametersMap.insert(key,value);
        endInsertRows();
    }

    return key;
}

void GrblConfigLogic::notifyOfPropertyChanges(int key)
{
    switch(key)
    {
    case GRBL_PARAM_REPORT_INCHES:
        emit GrblConfigIface::reportUnitIsInchChanged(getReportUnitIsInch());
        break;

    case GRBL_PARAM_X_MAX_RATE:
    case GRBL_PARAM_Y_MAX_RATE:
    case GRBL_PARAM_Z_MAX_RATE:
        emit GrblConfigIface::seekRatesChanged(getSeekRates());
        break;

    case GRBL_PARAM_X_MAX_ACC:
    case GRBL_PARAM_Y_MAX_ACC:
    case GRBL_PARAM_Z_MAX_ACC:
        emit GrblConfigIface::maxAccsChanged(getMaxAccs());
        break;

    case GRBL_PARAM_HOMING_ENABLED:
        emit GrblConfigIface::homingEnabledChanged(getHomingEnabled());
        break;

    case GRBL_PARAM_X_MAX_TRAVEL:
    case GRBL_PARAM_Y_MAX_TRAVEL:
    case GRBL_PARAM_Z_MAX_TRAVEL:
        emit GrblConfigIface::machineSpaceChanged(getMachineSpace());
        break;


    case ALL_PROPERTIES:
        emit GrblConfigIface::reportUnitIsInchChanged(getReportUnitIsInch());
        emit GrblConfigIface::seekRatesChanged(getSeekRates());
        emit GrblConfigIface::maxAccsChanged(getMaxAccs());
        emit GrblConfigIface::homingEnabledChanged(getHomingEnabled());
        emit GrblConfigIface::machineSpaceChanged(getMachineSpace());
        break;

    default:
        break;
    }
}

void GrblConfigLogic::clear()
{
    beginResetModel();
    m_parametersMap.clear();
    endResetModel();
}

bool GrblConfigLogic::getReportUnitIsInch()
{
    return qFloor(m_parametersMap.value(GRBL_PARAM_REPORT_INCHES, 0.0f)) != 0;
}

QVector3D GrblConfigLogic::getSeekRates()
{
    QVector3D seekRateVector(m_parametersMap.value(GRBL_PARAM_X_MAX_RATE, 0.0f),
                             m_parametersMap.value(GRBL_PARAM_Y_MAX_RATE, 0.0f),
                             m_parametersMap.value(GRBL_PARAM_Z_MAX_RATE, 0.0f));

    //Board returns seek rate mm per minutes, we want m per seconds
    return seekRateVector / 60.0f;

}

QVector3D GrblConfigLogic::getMaxAccs()
{
    QVector3D maxAccsVector(m_parametersMap.value(GRBL_PARAM_X_MAX_ACC, 0.0f),
                            m_parametersMap.value(GRBL_PARAM_Y_MAX_ACC, 0.0f),
                            m_parametersMap.value(GRBL_PARAM_Z_MAX_ACC, 0.0f));

    return maxAccsVector;
}

bool GrblConfigLogic::getHomingEnabled()
{
    return qFloor(m_parametersMap.value(GRBL_PARAM_HOMING_ENABLED, 0.0f)) != 0;
}



QVector3D GrblConfigLogic::getMachineSpace()
{
    QVector3D machineSpace(m_parametersMap.value(GRBL_PARAM_X_MAX_TRAVEL, 0.0f),
                           m_parametersMap.value(GRBL_PARAM_Y_MAX_TRAVEL, 0.0f),
                           m_parametersMap.value(GRBL_PARAM_Z_MAX_TRAVEL, 0.0f));

    return machineSpace;
}

int GrblConfigLogic::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
    {
        return 0;
    }

    return m_parametersMap.size();
}

QVariant GrblConfigLogic::data(const QModelIndex &index, int role) const
{
    if(index.parent().isValid() || index.column() != 0)
    {
        return QVariant();
    }

    //Now we are sure to have a valid index, ...
    // ... retrieve an iterator to the item
    int key = getKeyFromIndex(index.row());

    if(key < 0 )
    {
        return QVariant();
    }

    switch(role)
    {
    case KeyRole:
        return key;

    case ValueRole:
        return m_parametersMap.value(key);

    case LabelRole:
        return GrblStringRegister::getSettingsString(key,GrblStringRegister::Setting_Label);

    case UnitRole:
        return GrblStringRegister::getSettingsString(key,GrblStringRegister::Setting_Unit);

    case BitsRole:
        return GrblStringRegister::getSettingsString(key,GrblStringRegister::Setting_Bits).split(',');

    case DescriptionRole:
        return GrblStringRegister::getSettingsString(key,GrblStringRegister::Setting_Description);

    default:
        return QVariant();
    }
}

bool GrblConfigLogic::setData(const QModelIndex &index, const QVariant &value, int role)
{
    GrblBoard* board = qobject_cast<GrblBoard*>(parent());

    if(!board
    || index.parent().isValid()
    || index.column() != 0
    || role != ValueRole
    || !value.canConvert<QString>())
    {
        return false;
    }

    //Now we are sure to have a valid index, ...
    // ... retrieve an iterator to the item
    int key = getKeyFromIndex(index.row());

    if(key < 0)
    {
        return false;
    }

    //Immediately restore the previous value in the view, since it has not yet changed in the model
    emit dataChanged(index, index, {ValueRole} );

    //Build the grbl instruction to change the parameter
    QString instruction("$%1=%2");
    instruction = instruction.arg(QString::number(key));
    instruction = instruction.arg(value.toString());

    return board->executeSingleInstruction(instruction);
}


QHash<int, QByteArray> GrblConfigLogic::roleNames() const
{
    return QHash<int, QByteArray>(
    {
        {KeyRole,               "key"               },
        {ValueRole,             "value"             },
        {LabelRole,             "label"             },
        {UnitRole,              "unit"              },
        {BitsRole,              "bits"              },
        {DescriptionRole,       "description"       },
    });
}

int GrblConfigLogic::getKeyFromIndex(int index) const
{
    if(index >= m_parametersMap.size())
    {
        return -1;
    }

    QMap<int, double>::const_iterator itemIterator =
            m_parametersMap.constBegin() + index;

    return itemIterator.key();
}

int GrblConfigLogic::getIndexFromKey(int key) const
{
    int index = 0;
    for(QMap<int, double>::const_iterator itemIterator = m_parametersMap.constBegin();
        itemIterator != m_parametersMap.constEnd();
        itemIterator++)
    {
        if(key <= itemIterator.key())
        {
            break;
        }
        index++;
    }

    return index;
}

void GrblConfigLogic::onParameterMessageReceived(GrblMessage message)
{
    parseConfigurationLine(message.getText());
}

void GrblConfigLogic::onPotentiallyConfigInstructionSent(GrblInstructionPointer instPtr)
{
    //On configuration fetch instruction sent, clear known configuration
    if(instPtr->isConfigurationFetch())
    {
        clear();
    }
}

void GrblConfigLogic::onPotentiallyConfigInstructionCompleted(GrblInstructionPointer instPtr)
{
    //We only have to process GRBL-specific succesful instructions
    if(instPtr->getStatus() == GrblInstruction::Status_Ok)
    {

        //Reflect configuration changes from a configuration set instruction
        if(instPtr->isConfigurationSet())
        {
            int updatedKey = parseConfigurationLine(instPtr->getString());

            if(updatedKey != PARSING_ERROR)
            {
                notifyOfPropertyChanges(updatedKey);
            }
        }

        //On configuration fetch completed, notify of ALL properties changes
        else if(instPtr->isConfigurationFetch())
        {
            notifyOfPropertyChanges(ALL_PROPERTIES);
        }
    }
}

#include "grblabstractactivity.h"

#include "../grblboard.h"

GrblAbstractActivity::GrblAbstractActivity(GrblBoard *parent) : QObject(parent)
{
    //Default implementation : do nothing
}

void GrblAbstractActivity::onInstructionSent(GrblInstructionPointer)
{
    //Default implementation : do nothing
}

void GrblAbstractActivity::onInstructionCompleted(GrblInstructionPointer)
{
    //Default implementation : do nothing
}

void GrblAbstractActivity::onCompleted()
{
    //Default implementation : do nothing
}

GrblInstructionPointer GrblAbstractActivity::craftInstruction(QString instructionString, int line)
{
    GrblInstruction* createdInst = new GrblInstruction(instructionString, line);
    return GrblInstructionPointer(createdInst);
}

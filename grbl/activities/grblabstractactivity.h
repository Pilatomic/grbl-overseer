#ifndef GRBLABSTRACTACTIVITY_H
#define GRBLABSTRACTACTIVITY_H

#include <QObject>

#include "../grblinstruction.h"
#include "../grblengine.h"

#define NO_LINE_NUMBER_SPECIFIED   0

class GrblBoard;

class GrblAbstractActivity : public QObject
{
    Q_OBJECT
public:    
    explicit GrblAbstractActivity(GrblBoard *parent = nullptr);

    virtual void onInstructionSent(GrblInstructionPointer);
    virtual void onInstructionCompleted(GrblInstructionPointer);
    virtual void onCompleted();

    virtual GrblInstructionPointer getNextInstruction() = 0;

signals:
    void rtcmdToEngine(GrblEngine::RealTimeCommand cmd);

protected:
    static GrblInstructionPointer craftInstruction(QString instructionString,
                                                   int line = NO_LINE_NUMBER_SPECIFIED);
};

#endif // GRBLABSTRACTACTIVITY_H

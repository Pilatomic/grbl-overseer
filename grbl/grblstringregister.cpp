#include "grblstringregister.h"

#include <QDebug>

#define GRBL_STRING_PREFIX_SETTINGS                 "Settings/"
#define GRBL_STRING_PREFIX_ERRORS                   "Errors/"
#define GRBL_STRING_PREFIX_ALARMS                   "Alarms/"
#define GRBL_STRING_PREFIX_BUILDOPTIONS             "BuildOptions/"

#define GRBL_STRING_SETTING_LABEL_KEYSTRING         "_label"
#define GRBL_STRING_SETTING_UNIT_KEYSTRING          "_unit"
#define GRBL_STRING_SETTING_DESCRIPTION_KEYSTRING   "_desc"
#define GRBl_STRING_SETTING_BITS_KEYSTRING          "_bits"


QSettings* GrblStringRegister::s_stringFile = nullptr;

void GrblStringRegister::loadStringsFile(QString path)
{
    delete s_stringFile;
    s_stringFile = new QSettings(path, QSettings::IniFormat);
}

bool GrblStringRegister::isStringsAvailable()
{
    return !s_stringFile->allKeys().isEmpty();
}


QString GrblStringRegister::getSettingsString(quint8 settingKey, SettingsTextRole role)
{
    static const QMap<SettingsTextRole, QString> roleToKeyStringMap =
            QMap<SettingsTextRole, QString> (
    {
        {Setting_Label      , GRBL_STRING_SETTING_LABEL_KEYSTRING       },
        {Setting_Unit       , GRBL_STRING_SETTING_UNIT_KEYSTRING        },
        {Setting_Description, GRBL_STRING_SETTING_DESCRIPTION_KEYSTRING },
        {Setting_Bits       , GRBl_STRING_SETTING_BITS_KEYSTRING        }
    });

    QString keyString = QString::number(settingKey) +  roleToKeyStringMap.value(role);

    keyString.prepend(GRBL_STRING_PREFIX_SETTINGS);

    return s_stringFile->value(keyString).toString();
}

QString GrblStringRegister::getErrorString(quint8 errorId)
{
    QVariant value = s_stringFile->value(QStringLiteral(GRBL_STRING_PREFIX_ERRORS) + QString::number(errorId),
                                         QString("Unknown error, check GRBL wiki"));
    return value.toString();
}


QString GrblStringRegister::getAlarmString(quint8 alarmId)
{
    QVariant value = s_stringFile->value(QStringLiteral(GRBL_STRING_PREFIX_ALARMS) + QString::number(alarmId),
                                         QString("Unknown alarm, check GRBL wiki"));
    return value.toString();
}


QString GrblStringRegister::getBuildOptionString(unsigned char buildOptionChar)
{
    QVariant value = s_stringFile->value(QStringLiteral(GRBL_STRING_PREFIX_ALARMS) + buildOptionChar);
    return value.toString();
}



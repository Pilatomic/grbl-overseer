#include "grbldefinitions.h"
#include "grblstatuslogic.h"
#include "grblengine.h"
#include "grblconfiglogic.h"
#include "grblboard.h"

#include <QString>
#include <QStringList>
#include <QMap>
#include <QTimer>
#include <QDebug>

#define NO_EXPIRATION   -1

const QRegularExpression GrblStatusLogic::s_coordinatesExpression =
        QRegularExpression("(?<xvalue>-?[\\d]+\\.*[\\d]*),(?<yvalue>-?[\\d]+\\.*[\\d]*),(?<zvalue>-?[\\d]+\\.*[\\d]*)",
                           QRegularExpression::OptimizeOnFirstUsageOption);

const QString GrblStatusLogic::s_machinePositionString =
        QStringLiteral(STATUS_MACHINE_POS);

const QString GrblStatusLogic::s_workPositionString =
        QStringLiteral(STATUS_WORK_POS);

const QString GrblStatusLogic::s_wcoString =
        QStringLiteral(STATUS_WORK_COORD_OFFSET);

const QString GrblStatusLogic::s_buffersString =
        QStringLiteral(STATUS_BUFFERS);

const QString GrblStatusLogic::s_overridesString =
        QStringLiteral(STATUS_OVERRIDES);

const QString GrblStatusLogic::s_activeInputPins =
        QStringLiteral(STATUS_ACTIVE_INPUT_PINS);

const QString GrblStatusLogic::s_accessoryStateString =
        QStringLiteral(STATUS_ACCESSORY_STATE);

const QMap<QString, GrblStatusIface::State> GrblStatusLogic::s_stateDecodingMap =
        QMap<QString, GrblStatusIface::State>
        (
            {
                {STATE_IDLE_STRING      , GrblStatusIface::State_Idle       },
                {STATE_RUN_STRING       , GrblStatusIface::State_Run        },
                {STATE_HOLD_STRING      , GrblStatusIface::State_Hold       },
                {STATE_JOG_STRING       , GrblStatusIface::State_Jog        },
                {STATE_DOOR_STRING      , GrblStatusIface::State_Door       },
                {STATE_HOME_STRING      , GrblStatusIface::State_Home       },
                {STATE_ALARM_STRING     , GrblStatusIface::State_Alarm      },
                {STATE_CHECK_STRING     , GrblStatusIface::State_Check      },
                {STATE_SLEEP_STRING     , GrblStatusIface::State_Sleep      },
            }
        );

const QMap<QChar,GrblStatusIface::InputPin> GrblStatusLogic::s_inputPinDecodingMap =
        QMap<QChar, GrblStatusIface::InputPin>
        (
            {
                {INPUT_PIN_X_LIMIT      , GrblStatusIface::Input_X_Limit    },
                {INPUT_PIN_Y_LIMIT      , GrblStatusIface::Input_Y_Limit    },
                {INPUT_PIN_Z_LIMIT      , GrblStatusIface::Input_Z_Limit    },
                {INPUT_PIN_PROBE        , GrblStatusIface::Input_Probe      },
                {INPUT_PIN_DOOR         , GrblStatusIface::Input_Door       },
                {INPUT_PIN_HOLD         , GrblStatusIface::Input_Hold       },
                {INPUT_PIN_SOFT_RESET   , GrblStatusIface::Input_SoftReset  },
                {INPUT_PIN_CYCLE_START  , GrblStatusIface::Input_CycleStart },
            }
        );

const QMap<QChar,GrblStatusIface::AccessoryFlag> GrblStatusLogic::s_accessoryDecodingMap =
        QMap<QChar, GrblStatusIface::AccessoryFlag>
        (
            {
                {ACCESSORY_SPINDLE_CW       , GrblStatusIface::Accessory_Spindle_CW     },
                {ACCESSORY_SPINDLE_CCW      , GrblStatusIface::Accessory_Spindle_CCW    },
                {ACCESSORY_COOLANT_FLOOD    , GrblStatusIface::Accessory_Coolant_Flood  },
                {ACCESSORY_COOLANT_MIST     , GrblStatusIface::Accessory_Coolant_Mist   },
            }
        );

GrblStatusLogic::GrblStatusLogic(GrblEngine *engine, GrblBoard *parentBoard):
    GrblStatusIface(parentBoard),
    m_isReportUnitInches(false),
    m_grblEngine(engine)
{
    m_currValues.state = State_Offline;

    //Setup expiration timer
    m_expirationTimer = new QTimer(this);
    m_expirationTimer->setSingleShot(true);
    connect(m_expirationTimer, &QTimer::timeout,
            [=]() { setStateNoExpiration(State_Unknown); });

    //Setup status request timer
    m_statusTimer = new QTimer(this);
    m_statusTimer->start(STATUS_DEFAULT_REQUEST_INTERVAL);
    connect(m_statusTimer, &QTimer::timeout,
            this, &GrblStatusLogic::refresh);

    connect(engine, &GrblEngine::msgReceivedTypeStatus,
            this, &GrblStatusLogic::onStatusMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeWelcome,
            this, &GrblStatusLogic::refresh);
}

void GrblStatusLogic::onUnitIsInchesChanged(bool isInches)
{
    m_isReportUnitInches = isInches;
}

void GrblStatusLogic::setStateNoExpiration(GrblStatusLogic::State state)
{
    prepareNextParsing();
    m_currValues.state = state;

    m_expirationTimer->stop();

    if(m_prevValues.state != m_currValues.state)
    {
        emit currentStateChanged(m_currValues.state);
    }
}

void GrblStatusLogic::onStatusMessageReceived(GrblMessage message)
{   
    // 1. Current values are now previous values
    prepareNextParsing();

    // 2. Parse received status line
    parseStatusLine(message.getText());

    // 3. Some values need to be updated separately
    computePositions();

    // 4. Notify of changed properties
    notifyOfUpdatedProperties();

    // 5. Start expiration timer
    m_expirationTimer->start(STATUS_DEFAULT_EXPIRATION_INTERVAL);
}

void GrblStatusLogic::parseStatusLine(QString statusText)
{
    statusText.remove(RESPONSE_STATUS_START);
    statusText.remove(RESPONSE_STATUS_END);
    QVector<QStringRef> statusFields = statusText.splitRef(STATUS_FIELD_SEPARATOR,
                                                           QString::SkipEmptyParts);

    // 1st and 2nd fields are mandatory, if we don't have them, we clearly have an issue,
    // no need to get in deeper shit trying to parse this
    if(statusFields.size() < 2)
    {
        return;
    }

    //qDebug() << statusText;

    //First field is the state and is mandatory
    parseFieldState(statusFields.at(0));

    //Second field is also mandatory and is current position (machine pos OR work pos)
    parseFieldPosition(statusFields.at(1));

    //Then parse all the remaining fields ( 2 to end )
    for(QVector<QStringRef>::const_iterator currentFieldText = statusFields.cbegin() + 2;
        currentFieldText != statusFields.cend();
        currentFieldText++)
    {
        // The convoluted syntax below ensure parsing is attempted only
        // if field as not been parsed yet, and no over field as been parsed
        // during this iteration
        // To optimize parsing, sort parser by order of occurence
        // (more often used parser before less used)

        if(!m_currValues.parsedFields.testFlag(Field_WCO) &&
                parseFieldWCO(*currentFieldText))
        {
            //qDebug() << "WCO :" << parsedWco;
        }

        else if(!m_currValues.parsedFields.testFlag(Field_Overrides) &&
                parseFieldOverrides(*currentFieldText))
        {
            //qDebug() << "Overrides :" << m_currValues.overrides.feed << m_currValues.overrides.rapid << m_currValues.overrides.spindle;
        }

        else if(!m_currValues.parsedFields.testFlag(Field_ActiveInputPins) &&
                parseFieldActiveInputPins(*currentFieldText))
        { }

        else if(!m_currValues.parsedFields.testFlag(Field_AccessoryState) &&
                parseFieldAccessoryState(*currentFieldText))
        { }

        //ADD PARSING OF MORE FIELDS HERE ( BUFFER COMES LAST BECAUSE LEAST USED )

        else if(!m_currValues.parsedFields.testFlag(Field_Buffers) &&
                parseFieldBuffers(*currentFieldText))
        {
            //qDebug() << "Buffers :" << parsedBufferStatus;
        }
    }
    return;
}


bool GrblStatusLogic::parseFieldState(const QStringRef &stateField)
{
    foreach(const QString &stateString, s_stateDecodingMap.keys()){
        if(stateField.startsWith(stateString)){
            m_currValues.state = s_stateDecodingMap.value(stateString);
            m_currValues.parsedFields |= Field_State;
            return true;
        }
    }
    return false;
}

bool GrblStatusLogic::parseFieldPosition(const QStringRef &positionField)
{
    //Look for Machine position
    if(positionField.startsWith(s_machinePositionString)
    && parseTripleValues(positionField.mid(s_machinePositionString.length()),
                         &m_currValues.rawPosition))
    {
        m_currValues.isUnitInches = m_isReportUnitInches;
        m_currValues.parsedFields |= Field_MachinePos;
        return true;
    }

    //Look for work position
    else if(positionField.startsWith(s_workPositionString)
    && parseTripleValues(positionField.mid(s_workPositionString.length()),
                         &m_currValues.rawPosition))
    {
        m_currValues.isUnitInches = m_isReportUnitInches;
        m_currValues.parsedFields |= Field_WorkPos;
        return true;
    }

    return false;
}

bool GrblStatusLogic::parseFieldWCO(const QStringRef &fieldText)
{
    if(fieldText.startsWith(s_wcoString)
    && parseTripleValues(fieldText.mid(s_wcoString.length()), &m_currValues.wco))
    {
        m_currValues.parsedFields |= Field_WCO;
        return true;
    }

    return false;
}

bool GrblStatusLogic::parseFieldBuffers(const QStringRef &fieldText)
{
    if(fieldText.startsWith(s_buffersString))
    {
        QVector<QStringRef> bufferFields =  fieldText.mid(s_buffersString.size()).split(STATUS_BUFFER_SEPARATOR);

        if(bufferFields.size() >= 2)
        {
            parseSingleBufferField(bufferFields.at(0), &m_currValues.buffersStatus.planBuffer);
            parseSingleBufferField(bufferFields.at(1), &m_currValues.buffersStatus.charBuffer);
            m_currValues.parsedFields |= Field_Buffers;
            return true;
        }
    }
    return false;
}

void GrblStatusLogic::parseSingleBufferField(const QStringRef &bufferField, int* valueDest)
{
    bool success = false;
    int result = bufferField.toInt(&success);
    if(success) (*valueDest) = result;
}


bool GrblStatusLogic::parseFieldOverrides(const QStringRef &fieldText)
{
    QVector3D overridesVector;
    if(fieldText.startsWith(s_overridesString)
    && parseTripleValues(fieldText.mid(s_overridesString.length()), &overridesVector))
    {
        m_currValues.overrides.feed       = overridesVector.x();
        m_currValues.overrides.rapid      = overridesVector.y();
        m_currValues.overrides.spindle    = overridesVector.z();
        m_currValues.parsedFields |= Field_Overrides;

        //When Override field is present, we MUST RESET accessory state
        // cf GRBl documentation :
        // https://github.com/gnea/grbl/blob/master/doc/markdown/interface.md
        m_currValues.accessoryState = 0;

        return true;
    }

    return false;
}

bool GrblStatusLogic::parseFieldActiveInputPins(const QStringRef &fieldText)
{
    if(fieldText.startsWith(s_activeInputPins))
    {
        for(QStringRef::const_iterator i = fieldText.cbegin() + s_activeInputPins.length() ;
            i != fieldText.cend() ;
            i++)
        {
            m_currValues.activeInputPins |= s_inputPinDecodingMap.value(*i);
        }

        m_currValues.parsedFields |= Field_ActiveInputPins;
        return true;
    }

    return false;
}

bool GrblStatusLogic::parseFieldAccessoryState(const QStringRef &fieldText)
{
    if(fieldText.startsWith(s_accessoryStateString))
    {
        for(QStringRef::const_iterator i = fieldText.cbegin() ;
            i != fieldText.cend() ;
            i++)
        {
            m_currValues.accessoryState |= s_accessoryDecodingMap.value(*i);
        }

        m_currValues.parsedFields |= Field_AccessoryState;
        return true;
    }

    return false;
}

bool GrblStatusLogic::parseTripleValues(const QStringRef &coordinatesField, QVector3D *coordPtr)
{
    QRegularExpressionMatch coordMatch = s_coordinatesExpression.match(coordinatesField);
    if(coordMatch.hasMatch() && coordPtr){
        (*coordPtr) = QVector3D(coordMatch.captured("xvalue").toFloat(),
                             coordMatch.captured("yvalue").toFloat(),
                             coordMatch.captured("zvalue").toFloat());
        return true;
    }

    return false;
}

void GrblStatusLogic::computePositions()
{
    QVector3D workPosition, machPosition;

    //If we have machine pos, need to compute work pos
    if(m_currValues.parsedFields.testFlag(Field_MachinePos))
    {
        machPosition = m_currValues.rawPosition;
        workPosition = machPosition - m_currValues.wco;
    }

    //If we have work pos, need to compute machine pos
    else if(m_currValues.parsedFields.testFlag(Field_WorkPos))
    {
        workPosition = m_currValues.rawPosition;
        machPosition = workPosition + m_currValues.wco;
    }

    if(m_currValues.isUnitInches)
    {
        machPosition *= MM_PER_INCH;
        workPosition *= MM_PER_INCH;
    }

    m_currValues.machPositionMm = machPosition;
    m_currValues.workPositionMm = workPosition;

    return;
}

void GrblStatusLogic::notifyOfUpdatedProperties()
{
    if(m_currValues.state != m_prevValues.state)
        emit GrblStatusIface::currentStateChanged(m_currValues.state);

    if(m_currValues.machPositionMm != m_prevValues.machPositionMm)
        emit GrblStatusIface::machinePositionChanged(m_currValues.machPositionMm);

    if(m_currValues.workPositionMm != m_prevValues.workPositionMm)
        emit GrblStatusIface::workPositionChanged(m_currValues.workPositionMm);

    if(m_currValues.rawPosition != m_prevValues.rawPosition)
        emit GrblStatusIface::rawPositionChanged(m_currValues.rawPosition);

    if(m_currValues.isUnitInches != m_prevValues.isUnitInches)
        emit GrblStatusIface::isRawPositionInInchesChanged(m_currValues.isUnitInches);

    if(m_currValues.parsedFields.testFlag(Field_WorkPos) != m_prevValues.parsedFields.testFlag(Field_WorkPos))
        emit GrblStatusIface::isRawPositionInWorkCoordChanged(m_currValues.parsedFields.testFlag(Field_WorkPos));

    if(m_currValues.overrides.feed != m_prevValues.overrides.feed)
        emit GrblStatusIface::overrideFeedChanged(m_currValues.overrides.feed);

    if(m_currValues.overrides.rapid != m_prevValues.overrides.rapid)
        emit GrblStatusIface::overrideRapidChanged(m_currValues.overrides.rapid);

    if(m_currValues.overrides.spindle != m_prevValues.overrides.spindle)
        emit GrblStatusIface::overrideSpindleChanged(m_currValues.overrides.spindle);

    if(m_currValues.activeInputPins != m_prevValues.activeInputPins)
        emit GrblStatusIface::activeInputPinsChanged(m_currValues.activeInputPins);

    if(m_currValues.accessoryState != m_prevValues.accessoryState)
        emit GrblStatusIface::accessoryStateChanged(m_currValues.accessoryState);

    if(m_currValues.parsedFields.testFlag(Field_Buffers) != m_prevValues.parsedFields.testFlag(Field_Buffers))
        emit GrblStatusIface::hasBufferStatusChanged(m_currValues.parsedFields.testFlag(Field_Buffers));

    if(m_currValues.buffersStatus.planBuffer != m_prevValues.buffersStatus.planBuffer)
        emit GrblStatusIface::planBufferFreeSlotsChanged(m_currValues.buffersStatus.planBuffer);

    if(m_currValues.buffersStatus.charBuffer != m_prevValues.buffersStatus.charBuffer)
        emit GrblStatusIface::charBufferFreeSlotsChanged(m_currValues.buffersStatus.charBuffer);
}


void GrblStatusLogic::prepareNextParsing()
{
    m_prevValues = m_currValues;

    //Some value are considered 0 when empty. Those must be cleared now
    m_currValues.state = State_Unknown;
    m_currValues.buffersStatus = {-1,-1};
    m_currValues.activeInputPins = 0;
    m_currValues.parsedFields = 0;
}

void GrblStatusLogic::refresh()
{
    if(m_grblEngine->isAvailable())
    {
         m_grblEngine->pushRealTimeCommand(GrblEngine::RtCmd_StatusRequest);
    }
    else if(m_grblEngine->isOpened())
    {
        setStateNoExpiration(GrblStatusLogic::State_Unknown);
    }
    else
    {
        setStateNoExpiration(GrblStatusLogic::State_Offline);
    }
}

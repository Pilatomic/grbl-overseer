#ifndef GRBLSTATUSLOGIC_H
#define GRBLSTATUSLOGIC_H

#include "grblmessage.h"
#include "grblstatusiface.h"

#include <QObject>
#include <QVector3D>
#include <QRegularExpression>
#include <QTimer>
#include <QFlags>

#define STATUS_DEFAULT_REQUEST_INTERVAL     200
#define STATUS_DEFAULT_EXPIRATION_INTERVAL  500

class GrblConfigLogic;
class GrblBoard;
class GrblEngine;

class GrblStatusLogic : public GrblStatusIface
{
    Q_OBJECT

public:

    explicit GrblStatusLogic(GrblEngine *engine, GrblBoard *parentBoard);

    State getCurrentState() const override
        {return m_currValues.state;}
    State getPreviousState() const override
        {return m_prevValues.state;}

    QVector3D getMachinePositionInMm() const override
        {return m_currValues.machPositionMm;}

    QVector3D getWorkPositionInMm() const override
        {return m_currValues.workPositionMm;}

    QVector3D getRawPosition() const override
        {return m_currValues.rawPosition;}

    bool getIsRawPositionInInches() const override
        {return m_currValues.isUnitInches;}

    bool getIsRawPositionInWorkCoord() const override
        {return m_currValues.parsedFields.testFlag(Field_WorkPos);}

    bool getHasBufferStatus() const override
        {return m_currValues.parsedFields.testFlag(Field_Buffers);}

    int getPlanBufferFreeSlots() const override
        {return m_currValues.buffersStatus.planBuffer;}

    int getCharBufferFreeSlots() const override
        {return m_currValues.buffersStatus.charBuffer;}

    int getOverrideFeed() const override
        {return m_currValues.overrides.feed;}

    int getOverrideRapid() const override
        {return m_currValues.overrides.rapid;}

    int getOverrideSpindle() const override
        {return m_currValues.overrides.spindle;}

    InputPins getActiveInputPins() const override
        {return m_currValues.activeInputPins;}

    AccessoryFlags getAccessoryState() const override
        {return m_currValues.accessoryState;}

public slots:
    void refresh() override;
    void onUnitIsInchesChanged(bool isInches);

private slots:
    void onStatusMessageReceived(GrblMessage message);

private:
    enum Field
    {
        Field_State             = 1<<0,

        Field_MachinePos        = 1<<1,
        Field_WorkPos           = 1<<2,
        Field_WCO               = 1<<3,

        Field_OvrFeed           = 1<<4,
        Field_OvrRapid          = 1<<5,
        Field_OvrSpindle        = 1<<6,
        Field_Overrides         = Field_OvrFeed | Field_OvrRapid | Field_OvrSpindle,

        Field_Buffers           = 1<<7,

        Field_ActiveInputPins   = 1<<8,

        Field_AccessoryState    = 1<<9
    };

    struct OverridesStruct
    {
        int feed;
        int rapid;
        int spindle;
    };

    struct BuffersStruct
    {
        int planBuffer;
        int charBuffer;
    };

    struct ParsedValuesStruct
    {
        ParsedValuesStruct() :
            state(State_Unknown),
            rawPosition(),
            wco(),
            isUnitInches(false),
            buffersStatus({-1,-1}),
            overrides({100,100,100}),
            activeInputPins(0),
            accessoryState(0),
            parsedFields(0),
            machPositionMm(),
            workPositionMm()
        {}

        State           state;

        QVector3D       rawPosition;           //mPos or wPos
        QVector3D       wco;                //Work Coordinate Offset;
        bool            isUnitInches;

        BuffersStruct   buffersStatus;
        OverridesStruct overrides;
        InputPins       activeInputPins;
        AccessoryFlags  accessoryState;

        QFlags<Field>   parsedFields;

        QVector3D machPositionMm;
        QVector3D workPositionMm;
    };

    void setStateNoExpiration(State state);

    //Main parsing stages
    void parseStatusLine(QString statusText);
    void notifyOfUpdatedProperties();

    //Required fields parser
    bool parseFieldState(const QStringRef &stateField);
    bool parseFieldPosition(const QStringRef &positionField);

    //Optional fields parsers
    bool parseFieldWCO(const QStringRef &fieldText);
    bool parseFieldBuffers(const QStringRef &fieldText);
    bool parseFieldOverrides(const QStringRef &fieldText);
    bool parseFieldActiveInputPins(const QStringRef &fieldText);
    bool parseFieldAccessoryState(const QStringRef &fieldText);

    bool parseTripleValues(const QStringRef &coordinatesField, QVector3D *coordPtr);
    void parseSingleBufferField(const QStringRef &bufferField, int *valueDest);

    void computePositions();

    void prepareNextParsing();

    bool m_isReportUnitInches;

    ParsedValuesStruct m_currValues;
    ParsedValuesStruct m_prevValues;

    QTimer *m_expirationTimer;
    QTimer* m_statusTimer;

    GrblEngine* m_grblEngine;

    //Those are used by parser
    static const QMap<QString, State> s_stateDecodingMap;
    static const QRegularExpression s_coordinatesExpression;
    static const QString s_machinePositionString;
    static const QString s_workPositionString;
    static const QString s_wcoString;
    static const QString s_buffersString;
    static const QString s_overridesString;
    static const QString s_activeInputPins;
    static const QMap<QChar,InputPin> s_inputPinDecodingMap;
    static const QString s_accessoryStateString;
    static const QMap<QChar,AccessoryFlag> s_accessoryDecodingMap;
};

#endif // GRBLSTATUSLOGIC_H

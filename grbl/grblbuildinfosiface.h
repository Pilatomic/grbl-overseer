#ifndef GRBLBUILDINFOSIFACE_H
#define GRBLBUILDINFOSIFACE_H

#include <QObject>

class GrblBuildInfosIface : public QObject
{
    Q_OBJECT

    //build version
    Q_PROPERTY(QString buildString  READ getBuildString     NOTIFY buildStringChanged)

    //buffers depth
    Q_PROPERTY(int planBufferDepth  READ getPlanBufferDepth NOTIFY planBufferDepthChanged)
    Q_PROPERTY(int charBufferDepth  READ getCharBufferDepth NOTIFY charBufferDepthChanged)

    //build options
    Q_PROPERTY(BuildOptions options READ getOptions         NOTIFY optionsChanged)

public:

    enum BuildOption {
        Option_VariableSpindle                  = 1 << 0,
        Option_LineNumbers                      = 1 << 1,
        Option_MistCoolant                      = 1 << 2,
        Option_CoreXY                           = 1 << 3,
        Option_ParkingMotion                    = 1 << 4,
        Option_HomingForceOrigin                = 1 << 5,
        Option_HomingSingleAxis                 = 1 << 6,
        Option_TwoLimitSwitchesOnAxis           = 1 << 7,
        Option_SpindleDirPinAsEnablePin         = 1 << 8,
        Option_SpindleEnableOffWhenSpeedZero    = 1 << 9,
        Option_SoftwareLimitPinDebouncing       = 1 << 10,
        Option_ParkingOverrideControl           = 1 << 11,
        Option_FeedOverrideInProbeCycles        = 1 << 12,
        Option_RestoreAllEepromDisabled         = 1 << 13,
        Option_RestoreEeprom$SettingsDisabled   = 1 << 14,
        Option_RestoreEepromParamDataDisabled   = 1 << 15,
        Option_BuildInfoWriteUserStringDisabled = 1 << 16,
        Option_ForceSyncOpenEepromWriteDisabled = 1 << 17,
        Option_ForceSyncUponWcoChangeDisabled   = 1 << 18,
        Option_HomingInitAutoLockDisabled       = 1 << 19
    };

    Q_ENUMS(BuildOption)

    Q_DECLARE_FLAGS(BuildOptions, BuildOption)
    Q_FLAG(BuildOptions)

    explicit GrblBuildInfosIface(QObject *parent = 0) : QObject(parent) {}

    virtual QString getBuildString() const = 0;

    virtual int getPlanBufferDepth() const = 0;
    virtual int getCharBufferDepth() const = 0;

    virtual BuildOptions getOptions() const = 0;

signals:
    void buildStringChanged(QString string);
    void planBufferDepthChanged(int depth);
    void charBufferDepthChanged(int depth);
    void optionsChanged(BuildOptions options);

};

#endif // GRBLBUILDINFOSIFACE_H

#ifndef GRBLBOARD_H
#define GRBLBOARD_H

#include "grblinstruction.h"
#include "grblmessage.h"

#include <QObject>
#include <QVector3D>
#include <QIODevice>
#include <QTimer>
#include <QPointer>
#include <QVector>

class GCodeJob;
class GCodeJobLine;
class GrblEngine;
class GrblConfigIface;
class GrblConfigLogic;
class GrblStatusIface;
class GrblStatusLogic;
class GrblHistoryIface;
class GrblHistoryLogic;
class GrblBuildInfosIface;
class GrblBuildInfosLogic;

class GrblBoard : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool isIdle READ isIdle NOTIFY isIdleChanged)
    Q_PROPERTY(bool isJogAllowed READ isJogAllowed NOTIFY isJogAllowedChanged)
    Q_PROPERTY(bool isSimulationMode READ isSimulationMode WRITE setSimulationMode NOTIFY isSimulationModeChanged)
    Q_PROPERTY(HomingState homingState READ getHomingState  NOTIFY homingStateChanged)

public:
    enum HomingState {Homing_Invalid, Homing_Running, Homing_Valid};
    Q_ENUMS(HomingState)

    enum Overrides
    {
        Ovr_Feed_Normal, Ovr_Feed_Inc, Ovr_Feed_Dec, Ovr_Feed_Inc_Fine, Ovr_Feed_Dec_Fine,
        Ovr_Rapid_Normal, Ovr_Rapid_Slow, Ovr_Rapid_Slower,
        Ovr_Spindle_Normal, Ovr_Spindle_Inc, Ovr_Spindle_Dec, Ovr_Spindle_Inc_Fine, Ovr_Spindle_Dec_Fine,
    };
    Q_ENUMS(Overrides)

    explicit GrblBoard(QObject *parent = 0);

    GrblStatusIface *status(void);

    GrblHistoryIface *history(void);

    GrblConfigIface *configuration(void);

    GrblBuildInfosIface *buildInfos(void);

    bool isIdle() const
        {return m_isIdle;}

    bool isJogAllowed() const
        {return m_isJogAllowed;}

    bool isSimulationMode() const
        {return m_isSimulation;}

    HomingState getHomingState() const
        {return m_homingState;}

    const GCodeJob* getCurrentJob() const;

signals:
    void jobCompleted(GCodeJob* job);
    void isIdleChanged();
    void isJogAllowedChanged();
    void isSimulationModeChanged();
    void homingStateChanged(HomingState homingState);

    void boardVersionNotSupported();

    void error(QString errorMsg);
    void alarm(QString alarmMsg);
    //void boardResetSequenceCompleted();

public slots:

    void reset();
    void hold();
    void resume();
    void homing();
    void killAlarm();

    //Toggle spindle. Only works in HOLD state
    void toggleSpindleStop();

    //Toggle mist coolant.  Only works in IDLE, RUN, or HOLD states
    void toggleCoolantMist();

    //Toggle flood coolant.  Only works in IDLE, RUN, or HOLD states
    void toggleCoolantFlood();

    void applyOverride(Overrides override);

    void setLinkDevice(QIODevice *device);

    bool runJob(GCodeJob* job);

    bool jog(QVector3D jogVector);  //jobVector is expressed as fraction of seek velocity per axis

    bool setSimulationMode(bool enable);

    bool executeSingleInstruction(QString instructionString);

private slots:
    //void onStatusStateUpdated();

    void onWelcomeMsgReceived(const GrblMessage &message);
    void onErrorMsgReceived(const GrblMessage &message);

    void onStartupActivityCompleted();

    void updateFlags();

private:
    bool executeStartupInstructions(void);
    void setHomingState(HomingState state);
    void setIsSimulation(bool simulation);

    bool m_isIdle;
    bool m_isJogAllowed;
    bool m_isSimulation;

    HomingState m_homingState;

    GrblEngine *m_engine;
    GrblConfigLogic *m_config;
    GrblStatusLogic *m_status;
    GrblHistoryLogic* m_history;
    GrblBuildInfosLogic* m_buildInfos;

    // Ugly hack to make simulation working.
    // Normally we determine job offset using actual coordinates.
    // But simulation mode uses its own coordinates set.
    // So we must retain the current "simulation" position, ...
    // .. to send the correct "set offset" command
    QVector3D m_simulationPosition;

    static const QVector<QString> s_startupInstructionList;
};

#endif // GRBLBOARD_H

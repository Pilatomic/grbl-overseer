#ifndef GRBLHISTORYIFACE_H
#define GRBLHISTORYIFACE_H

#include <QAbstractListModel>

class GrblBoard;

class GrblHistoryIface : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Status
    {
        Status_None,
        Status_Sent, Status_Ok, Status_Error, Status_Aborted,
        Status_Alarm
    };

    Q_ENUMS(Status)

    enum ItemRoles
    {
        StatusRole = Qt::UserRole + 1,      // type : enum Statuses
        MainTextRole,                       // type : QString
        SubTextsRole                        // type : QStringList
    };

    explicit GrblHistoryIface(QObject *parent = 0) : QAbstractListModel(parent) {}


public: // QAbstractItemModel interface
    virtual int rowCount(const QModelIndex &parent) const override = 0;
    virtual QVariant data(const QModelIndex &index, int role) const override = 0;

signals:
    void messageAddedToItem(int index);
};

#endif // GRBLHISTORYIFACE_H

#ifndef RENDERABLECENTERINDICATOR_H
#define RENDERABLECENTERINDICATOR_H

#include "renderableobject.h"

class RenderableCenterIndicator : public RenderableObject
{
public:
    RenderableCenterIndicator();
};

#endif // RENDERABLECENTERINDICATOR_H

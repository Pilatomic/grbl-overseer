#ifndef SCOPEDBINDERS_H
#define SCOPEDBINDERS_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

class ScopedAttributeArrayBinder
{
public:
    explicit ScopedAttributeArrayBinder();
    explicit ScopedAttributeArrayBinder(QOpenGLShaderProgram *program, const char * name,
                                    GLenum type, int offset, int tupleSize, int stride = 0);
    ~ScopedAttributeArrayBinder();
private:
    int m_attributeNumber;
    QOpenGLShaderProgram *m_program;
};

//##########################################################################

class ScopedVertexBufferBinder
{
public:
    explicit ScopedVertexBufferBinder(QOpenGLBuffer* bufferToBind = nullptr);
    ~ScopedVertexBufferBinder();

    bool isBindingSuccess();

private:
    QOpenGLBuffer* m_buffer;
};

//##########################################################################

class ScopedTextureBinder
{
public:
    explicit ScopedTextureBinder(QOpenGLShaderProgram *program, const char * name, QOpenGLTexture *texture);
    ~ScopedTextureBinder();
private:
    QOpenGLTexture* m_texture;
    static GLuint m_textureUnit;
};


#endif // SCOPEDBINDERS_H

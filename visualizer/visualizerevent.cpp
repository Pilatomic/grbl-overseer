#include "visualizerevent.h"

VisualizerEvent::VisualizerEvent() :
    m_type(Unknown),
    m_jobPointer(nullptr),
    m_int1(-1),
    m_int2(-1)
{

}

VisualizerEvent::Type VisualizerEvent::getType() const
{
    return m_type;
}



const GCodeJob *VisualizerEvent::getJobPointer() const
{
    return m_jobPointer;
}

GCodeJobLine::Status VisualizerEvent::getLineStatus() const
{
    return m_status;
}

int VisualizerEvent::getInt1() const
{
    return m_int1;
}

void VisualizerEvent::setInt2(int int2)
{
    m_int2 = int2;
}

int VisualizerEvent::getInt2() const
{
    return m_int2;
}

QVector3D VisualizerEvent::getPosition() const
{
    return m_position;
}

QColor VisualizerEvent::getColor() const
{
    return m_color;
}

VisualizerEvent VisualizerEvent::createJobCreatedEvent(const GCodeJob *jobPointer)
{
    VisualizerEvent event;
    event.m_type = JobCreated;
    event.m_jobPointer = jobPointer;
    return event;
}

VisualizerEvent VisualizerEvent::createJobDeletedEvent(const GCodeJob *jobPointer)
{
    VisualizerEvent event;
    event.m_type = JobDeleted;
    event.m_jobPointer = jobPointer;
    return event;
}

VisualizerEvent VisualizerEvent::createJobLineStatusChangedEvent(const GCodeJob *jobPointer, int line, GCodeJobLine::Status status)
{
    VisualizerEvent event;
    event.m_type = JobLineStatusChanged;
    event.m_jobPointer = jobPointer;
    event.m_int1 = line;
    event.m_int2 = line;
    event.m_status = status;
    return event;
}

VisualizerEvent VisualizerEvent::createJobPositionChangedEvent(const GCodeJob *jobPointer, QVector3D position)
{
    VisualizerEvent event;
    event.m_type = JobPositionChanged;
    event.m_jobPointer = jobPointer;
    event.m_position = position;
    return event;
}

VisualizerEvent VisualizerEvent::createJobColorChangedEvent(const GCodeJob *jobPointer, QColor color)
{
    VisualizerEvent event;
    event.m_type = JobColorChanged;
    event.m_jobPointer = jobPointer;
    event.m_color = color;
    return event;
}

VisualizerEvent VisualizerEvent::createSelectedJobChangedEvent(const GCodeJob *jobPointer)
{
    VisualizerEvent event;
    event.m_type = SelectedJobChanged;
    event.m_jobPointer = jobPointer;
    return event;
}

VisualizerEvent VisualizerEvent::createMachinePositionChangedEvent(QVector3D position)
{
    VisualizerEvent event;
    event.m_type = MachinePositionChanged;
    event.m_position = position;
    return event;
}

VisualizerEvent VisualizerEvent::createMachineSpaceChangedEvent(QVector3D position)
{
    VisualizerEvent event;
    event.m_type = MachineSpaceChanged;
    event.m_position = position;
    return event;
}

VisualizerEvent VisualizerEvent::createSpindleRotationChangedEvent(int direction)
{
    VisualizerEvent event;
    event.m_type = SpindleRotationChanged;
    event.m_int1 = direction;
    return event;
}

VisualizerEvent VisualizerEvent::createReportImperialUnitChangedEvent(bool imperialUnits)
{
    VisualizerEvent event;
    event.m_type = ReportImperialUnitsChanged;
    event.m_int1 = imperialUnits;
    return event;
}


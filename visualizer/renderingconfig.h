#ifndef RENDERINGCONFIG_H
#define RENDERINGCONFIG_H

#include <QOpenGLFunctions> //To access GLenum
#include <QVector4D>
#include <QMatrix4x4>

struct RenderingConfig
{
    enum ColorMode
    {
        ColorMode_DirectColor =0,
        ColorMode_TextureCoord
    };

    enum DrawMode
    {
        DrawMode_Skip           = 0,
        DrawMode_Lines          = GL_LINES,
        DrawMode_LineStrip      = GL_LINE_STRIP,
        DrawMode_Triangles      = GL_TRIANGLES,
        DrawMode_TriangleStrip  = GL_TRIANGLE_STRIP
    };

    DrawMode drawMode;
    GLint firstVertex;
    GLsizei vertexCount;
    bool useIndices;
    ColorMode colorMode;
    float lineWidth;
    int mMatrixIndex;
    QVector4D colorAFactor;
    QVector4D colorBFactor;

    RenderingConfig() :
        drawMode(DrawMode_Skip),
        firstVertex(0),
        vertexCount(0),
        useIndices(false),
        colorMode(ColorMode_DirectColor),
        lineWidth(0.0f),
        mMatrixIndex(0)
    {}
};


#endif // RENDERINGCONFIG_H

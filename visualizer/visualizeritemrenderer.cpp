#include "visualizeritemrenderer.h"
#include "visualizeritem.h"
#include "renderableaxes.h"
#include "renderablejob.h"
#include "renderablespindle.h"
#include "renderablemachinespace.h"
#include "renderablejobhighlighter.h"
#include "renderablecenterindicator.h"
#include "renderablerotationindicator.h"

#include <QOpenGLFramebufferObjectFormat>
#include <QtMath>
#include <QMap>
#include <QElapsedTimer>


#define ZNEAR   -10000.0f
#define ZFAR    10000.0f

//#define MEASURE_RENDER_TIME
//#define MEASURE_SYNC_TIME

VisualizerItemRenderer::VisualizerItemRenderer(int quality) :
    m_program(nullptr),
    m_quality(quality)
{
    initializeOpenGLFunctions();

    m_program = new QOpenGLShaderProgram();

    // Compile vertex & fragment shaders, then link
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vshader");
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fshader");
    m_program->link();

    //m_axes = new RenderableAxes();

    m_drill = new RenderableSpindle(20.0f, 2.0f);
    //m_drill->setRotation(1);  //UNCOMMENT TO DEBUG DRILL ROTATION

    m_machineSpace = new RenderableMachineSpace();
    //m_machineSpace->setLimit(QVector3D(-180.0f,-130.0f,-40.0f));   //UNCOMMENT TO DEBUG MACHINE SPACE

    m_jobHighlighter = new RenderableJobHighlighter();

    m_centerIndicator = new RenderableCenterIndicator();

    m_rotationIndicator = new RenderableRotationIndicator();

    //qDebug()<< "Renderer constructed";
}

VisualizerItemRenderer::~VisualizerItemRenderer()
{
    //qDebug()<< "Renderer destructed";
    qDeleteAll(m_jobs);
    //delete m_axes;
    delete m_drill;
    delete m_machineSpace;
    delete m_jobHighlighter;
    delete m_centerIndicator;
    delete m_rotationIndicator;
    delete m_program;
}

QOpenGLFramebufferObject* VisualizerItemRenderer::createFramebufferObject(const QSize &size)
{
    //qDebug()<< "Renderer asked to create FBO";
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    if (m_quality > 0) format.setSamples(4);   //Multisampling (AA)
    return new QOpenGLFramebufferObject(size, format);
}

void VisualizerItemRenderer::synchronize(QQuickFramebufferObject *item) {

#ifdef MEASURE_SYNC_TIME
    static QElapsedTimer syncElapsedTime;
    syncElapsedTime.start();
#endif

    VisualizerItem *vitem = static_cast<VisualizerItem *>(item);

    processViewData(vitem->getViewData());

    //Process events
    while(vitem->hasEventsAvailable())
    {
        VisualizerEvent event = vitem->takeEvent();
        processJobEvent(event);
    }

    m_window = vitem->window(); //Useful to restore openGL context

    //qDebug()<< "Renderer sync";

#ifdef MEASURE_SYNC_TIME
    qint64 syncDuration = syncElapsedTime.elapsed();
    qDebug() << "Sync duration" << syncDuration;
#endif
}

void VisualizerItemRenderer::processJobEvent(VisualizerEvent &event)
{
    const GCodeJob* jobPtr = event.getJobPointer();
    RenderableJob* jobRenderPtr = m_jobs.value(jobPtr,nullptr);

    switch(event.getType())
    {
    case VisualizerEvent::JobCreated:
    {
        delete m_jobs.take(jobPtr); //Safe, if object does not exist in map it will return 0
        m_jobs.insert(jobPtr, new RenderableJob(jobPtr));
    }
    break;

    case VisualizerEvent::JobDeleted:
    {
        //Safe, if object does not exist in map it will return 0
        RenderableJob* renderableJobToDelete =  m_jobs.take(jobPtr);

        if(m_jobHighlighter->getSelectedJob() == renderableJobToDelete)
        {
            m_jobHighlighter->setHighlightedJob(nullptr);
        }

        delete renderableJobToDelete;
    }
    break;

    case VisualizerEvent::JobLineStatusChanged:
    {
        if(!jobRenderPtr) break;
        jobRenderPtr->updateLineStatus(event.getInt1(), event.getInt2(),
                                    event.getLineStatus());
    }
    break;

    case VisualizerEvent::JobPositionChanged:
    {
        if(!jobRenderPtr) break;
        jobRenderPtr->setOffset(event.getPosition());
        if(m_jobHighlighter->getSelectedJob() == jobRenderPtr)
        {
            m_jobHighlighter->updateOffset();
        }
    }
    break;

    case VisualizerEvent::JobColorChanged:
    {
        if(!jobRenderPtr) break;
        jobRenderPtr->setColor(event.getColor());
    }
    break;

    case VisualizerEvent::SelectedJobChanged:
    {
        RenderableJob* selectedRenderableJob = m_jobs.value(jobPtr, nullptr);
        m_jobHighlighter->setHighlightedJob(selectedRenderableJob);
    }
    break;

    case VisualizerEvent::MachinePositionChanged:
    {
        m_drill->setPosition(event.getPosition());
    }
    break;

    case VisualizerEvent::MachineSpaceChanged:
    {
        m_machineSpace->setLimit(event.getPosition());
    }
    break;

    case VisualizerEvent::SpindleRotationChanged:
    {
        m_drill->setRotation(event.getInt1());
    }
    break;

    case VisualizerEvent::ReportImperialUnitsChanged:
    {
        m_jobHighlighter->setUseImperialUnits(event.getInt1());
    }

    case VisualizerEvent::Unknown:
        break;
    }
}

void VisualizerItemRenderer::render3dElements()
{
    //render axes & drill
//    m_axes->render(m_program);
    m_drill->render(m_program);

    m_jobHighlighter->render(m_program);

    //Render jobs
    for(QMap<const GCodeJob*,RenderableJob*>::const_iterator i = m_jobs.constBegin() ;
        i != m_jobs.constEnd() ;
        i++)
    {
        i.value()->render(m_program);
    }

    //render machine space
    m_machineSpace->render(m_program);
}

void VisualizerItemRenderer::render2dElements()
{
    m_centerIndicator->render(m_program);
    m_rotationIndicator->render(m_program);
}

void VisualizerItemRenderer::processViewData(const ViewData *newViewData)
{
    if(m_lastViewData != (*newViewData))
    {
        //update 2D matrix
        float w = newViewData->viewPortSize.x() / 2 ;
        float h = newViewData->viewPortSize.y() / 2;

        m_2dVpMatrix.setToIdentity();
        m_2dVpMatrix.ortho(-w,w,h,-h,ZFAR,ZNEAR);

        //Update 3D matrix
        QMatrix4x4 viewMatrix;
        viewMatrix.rotate(newViewData->rotation);
        viewMatrix.scale(1/newViewData->zoomLevel);
        viewMatrix.translate(newViewData->translation);

        m_3dVpMatrix = m_2dVpMatrix * viewMatrix;

        //Update rotation indicator
        m_rotationIndicator->updatePositionAndRotation(newViewData->viewPortSize, newViewData->rotation);

        m_lastViewData = *newViewData;
    }
}


void VisualizerItemRenderer::render()
{
#ifdef MEASURE_RENDER_TIME
    static QElapsedTimer renderElapsedTime;
    renderElapsedTime.start();
#endif

    bool scheduleAnotherRender = false;

    scheduleAnotherRender = m_drill->update();

    m_program->bind();
    m_program->setUniformValue("vp_matrix",m_3dVpMatrix);

    //Only show triangles drawn in CCW
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);

    //Allow depth testing, far is Z = 0.0f, so closer mean Z greater
    glEnable(GL_DEPTH_TEST);

    //No blending for the moment
    glDisable(GL_BLEND);

    //Clear buffers
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepthf(0.0f);
    glClearStencil(0);

    //3D 1ST PASS RENDER : RENDER ONLY TO DEPTH BUFFER TO DETERMINE Z-ORDER
    glDisable(GL_STENCIL_TEST);

    glDepthFunc(GL_GREATER);
    glDepthMask(GL_TRUE);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    render3dElements();

    //3D 2ND PASS RENDER : DRAW BACKGROUND COLOR
    // We use stencil buffer to avoid drawing 2 times at the same pixel :
    // it would produce ugly alpha accumulation
    glEnable(GL_STENCIL_TEST);
    glEnable(GL_BLEND);

    glStencilFunc( GL_NOTEQUAL, 1, 0xFFFF );
    glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
    glDepthFunc(GL_LESS);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glStencilMask(0xff);
    glDepthMask(GL_FALSE);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

    glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    render3dElements();

    //3D 3RD PASS RENDER : DRAW TOP COLOR
    // We use stencil buffer to avoid drawing 2 times at the same pixel :
    // it would produce ugly alpha accumulation

    glDepthFunc(GL_EQUAL);

    glClear(GL_STENCIL_BUFFER_BIT);

    render3dElements();

    //Now render 2D elements
    m_program->setUniformValue("vp_matrix",m_2dVpMatrix);

    glDisable(GL_STENCIL_TEST);
    glDisable(GL_DEPTH_TEST);

    render2dElements();

    m_program->release();


    // Not strictly needed for this example, but generally useful for when
    // mixing with raw OpenGL.
    if(m_window)
    {
        m_window->resetOpenGLState();
    }

    if(scheduleAnotherRender)
    {
        update();
    }

#ifdef MEASURE_RENDER_TIME
    qint64 renderDuration = renderElapsedTime.elapsed();
    qDebug() << "Render duration" << renderDuration
             << ", update =" << (scheduleAnotherRender ? "1" : "0");
#endif
}

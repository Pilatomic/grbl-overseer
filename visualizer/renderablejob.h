#ifndef RENDEREDJOB_H
#define RENDEREDJOB_H

#include <gcode/gcodejobline.h>
#include "renderableobject.h"
#include <QMap>

class GCodeJob;


class RenderableJob : public RenderableObject
{
public:
    explicit RenderableJob(const GCodeJob* job);

    void updateLineStatus(int fromLine, int toLine, GCodeJobLine::Status lineStatus);
    void setColor(QColor color);
    void setOffset(QVector3D offset);

    void render(QOpenGLShaderProgram *program) override;
    void renderHalo(QOpenGLShaderProgram *program);

    QVector3D getLimitMin() const;
    QVector3D getLimitMax() const;
    QVector3D getOffset() const;

protected:
    enum VertexStatusEnum
    {
        VertexStatus_Ready = 0,
        VertexStatus_Queued,
        VertexStatus_Sent,
        VertexStatus_Accepted,
        VertexStatus_Refused,
        VertexStatusEnum_Count
    };

    enum VertexTypeEnum
    {
        VertexType_Work = 0,
        VertexType_Move = VertexStatusEnum_Count,
        VertexTypeEnum_Count
    };

    struct LineVerticesStruct{
        GLint firstVertexIndex;
        GLsizei vertexCount;
        VertexTypeEnum vertexType;
    };

    QVector<LineVerticesStruct> m_verticesInfos;

    QVector4D getLineColor(VertexTypeEnum lineType, GCodeJobLine::Status lineStatus);

private:
    QVector3D m_offset;
    QVector3D m_limitMin;
    QVector3D m_limitMax;

    static const QMap<GCodeJobLine::Status,RenderableJob::VertexStatusEnum> s_lineStatusToVertexStatusMap;
    static const QVector<QVector4D> s_colorPalette;
};

#endif // RENDEREDJOB_H

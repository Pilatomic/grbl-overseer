#ifndef TOUCHPOINTSMANAGER_H
#define TOUCHPOINTSMANAGER_H

#include <QObject>
#include <QVector2D>
#include <QVector>
#include <QTouchEvent>

class TouchPointsManager : public QObject
{
    Q_OBJECT

public:
    TouchPointsManager(QObject *parent = nullptr);

    void processEvent(QTouchEvent *event);

    QVector2D   getPanVector();
    float       getPinchScaleRatio();
    QVector2D   getPinchCenterMovementVector();
    float       getPinchRotationAngle();

signals:
    void panned(QVector2D vector);
    void pinchScaled(float);
    void pinchMoved(QVector2D vector);
    void pinchRotated(float angle);
    void doublePress();

private:
    void appendTouchPoint(int id, QVector2D pos);
    void updateTouchPoint(int id, QVector2D pos);
    void removeTouchPoint(int id);

    struct TouchPointStruct {

        TouchPointStruct(int id = 0, QVector2D pos = QVector2D())
        {
            this->id = id;
            this->currPos = pos;
            this->prevPos = pos;
        }

        int id;
        QVector2D currPos;
        QVector2D prevPos;
    };

    QVector<TouchPointStruct> m_touchPoints;

    QVector2D m_doublePressPos;
    QTimer* m_doublePressTimer;

    enum DoublePressStage {DoublePress_Wait, DoublePress_Down, DoublePress_Up, DoublePress_Completed, DoublePress_Notified };
    DoublePressStage m_doublePressStage;
};


#endif // TOUCHPOINTSMANAGER_H

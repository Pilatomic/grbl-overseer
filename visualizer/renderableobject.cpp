#include "renderableobject.h"
#include "scopedbinders.h"

#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QColor>


RenderableObject::RenderableObject(int rndrConfCount, int matricesCount):
    m_renderingConfigs(rndrConfCount),
    m_mMatrices(matricesCount),
    m_texture(nullptr),
    m_vertexBuffer(nullptr),
    m_indexBuffer(nullptr)
{
    initializeOpenGLFunctions();
}


void RenderableObject::render(QOpenGLShaderProgram *program)
{
    for(int i = 0 ; i< m_renderingConfigs.size() ; i++)
    {
        performRendering(program, i);
    }
}

void RenderableObject::createVertexBuffer(int verticeCount, const VertexData *data)
{
    if(m_vertexBuffer) delete m_vertexBuffer;
    if(verticeCount <= 0) return;

    int size = verticeCount * sizeof(VertexData);
    m_vertexBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_vertexBuffer->create();
    ScopedVertexBufferBinder bufferBinded(m_vertexBuffer);
    (data) ? m_vertexBuffer->allocate(data,size) : m_vertexBuffer->allocate(size);
}

void RenderableObject::writeVertexBuffer(quint32 index, GLsizei size, const void *data)
{
    ScopedVertexBufferBinder bufferBinded(m_vertexBuffer);
    m_vertexBuffer->write(index, data, size);
}

void RenderableObject::createIndexBuffer(int verticeCount, const GLushort *data)
{
    if(m_indexBuffer) delete m_indexBuffer;
    if(verticeCount <= 0) return;

    int size = verticeCount * sizeof(GLushort);
    m_indexBuffer = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    m_indexBuffer->create();
    ScopedVertexBufferBinder bufferBinded(m_indexBuffer);
    (data) ? m_indexBuffer->allocate(data,size) : m_indexBuffer->allocate(size);
}

void RenderableObject::writeIndexBuffer(quint32 index, GLsizei size, const void *data)
{
    ScopedVertexBufferBinder bufferBinded(m_indexBuffer);
    m_indexBuffer->write(index, data, size);
}

void RenderableObject::performRendering(QOpenGLShaderProgram *program, int configIndex)
{
    const RenderingConfig &renderingConfig = m_renderingConfigs.at(configIndex);

    if(renderingConfig.drawMode == RenderingConfig::DrawMode_Skip) return;

    bool isUsingTexture = (renderingConfig.colorMode ==
                           RenderingConfig::ColorMode_TextureCoord);

    // Set per objects uniforms
    const QMatrix4x4 &mMatrix = m_mMatrices.at(renderingConfig.mMatrixIndex);
    program->setUniformValue("m_matrix", mMatrix);
    program->setUniformValue("color_a", renderingConfig.colorAFactor);
    program->setUniformValue("color_b", renderingConfig.colorBFactor);
    program->setUniformValue("textured", isUsingTexture);


    ScopedTextureBinder bindedTexture(program, "texture",
                                      isUsingTexture ? m_texture : nullptr);

    ScopedVertexBufferBinder bindedVertexBuffer(m_vertexBuffer);

    // Offset for position
    ScopedAttributeArrayBinder bindedPositionAttribute(
                program,
                "a_position",                               // Name in shader
                GL_FLOAT,                                   // Type
                offsetof(VertexData,position),              // Offset
                3,                                          // Number of component
                sizeof(VertexData));                        // Bytes before next value

    // Offset for static color index
    ScopedAttributeArrayBinder bindedColorAttribute(
                program,
                "a_colorinfo",                              // Name in shader
                GL_FLOAT,                                   // Type
                offsetof(VertexData,colorInfo),             // Offset
                4,                                          // Number of component
                sizeof(VertexData));                        // Bytes before next value


    //Draw
    glLineWidth(renderingConfig.lineWidth);

    if(renderingConfig.useIndices)
    {
        ScopedVertexBufferBinder bindedIndexBuffer(m_indexBuffer);
        glDrawElements(renderingConfig.drawMode,
                       renderingConfig.vertexCount,
                       GL_UNSIGNED_SHORT,
                       (GLvoid*)(renderingConfig.firstVertex * sizeof(GLushort)));
    }
    else
    {
        glDrawArrays(renderingConfig.drawMode,
                     renderingConfig.firstVertex,
                     renderingConfig.vertexCount);
    }
}


QVector4D RenderableObject::colorToVector4D(QColor color)
{
    return QVector4D(color.redF(),
                     color.greenF(),
                     color.blueF(),
                     color.alphaF());
}

void RenderableObject::setTexture(QOpenGLTexture *texture)
{
    if(m_texture) delete m_texture;
    m_texture = texture;
}

const QMatrix4x4 RenderableObject::generateMMatrix(QVector3D position, QQuaternion rotation, float scale)
{
    QMatrix4x4 matrix;
    matrix.translate(position);
    matrix.rotate(rotation);
    matrix.scale(scale);
    return matrix;
}

RenderingConfig &RenderableObject::getRndrConfRef(int i)
{
    return m_renderingConfigs[i];
}

QMatrix4x4 &RenderableObject::getMMatrixRef(int i)
{
    return m_mMatrices[i];
}


RenderableObject::~RenderableObject()
{
    delete m_vertexBuffer;
    delete m_indexBuffer;
    delete m_texture;
}

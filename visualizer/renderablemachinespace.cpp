#include "renderablemachinespace.h"
#include <QtMath>
#include <QDebug>
#include <QImage>
#include <QOpenGLTexture>

#define TEXTURE_STRIPES_WIDTH_MM    10.0f

#define COLOR1                      0x01003030
#define COLOR2                      0x10404040

#define INDICES_TEXTURED_FIRST      0
#define INDICES_TEXTURED_COUNT      34
#define INDICES_WIREFRAME_FIRST     34
#define INDICES_WIREFRAME_COUNT     16

const QVector<GLushort> RenderableMachineSpace::s_indices = QVector<GLushort>(
{
//Textured cube (Gl_TRIANGLES)
    0,  1,  2,  3,  3,     // Bottom
    4,  4,  5,  6,  7,  7, // Top
    8,  8,  9, 10, 11, 11, // Back
   12, 12, 13, 14, 15, 15, // Front
   16, 16, 17, 18, 19, 19, // Left
   20, 20, 21, 22, 23,     // Right
//Wireframe cube (GL_LINE_STRIP)
    0,  5,  4,  6,  3,  1,  0,  2,
    7,  5,  4,  1,  3,  2,  7,  6
});

RenderableMachineSpace::RenderableMachineSpace() :
    RenderableObject(2, 1)
{
    QImage textureImage(2,2,QImage::Format_ARGB32);

    //Standard pattern
    textureImage.setPixel(0,0,COLOR1);
    textureImage.setPixel(1,0,COLOR2);
    textureImage.setPixel(0,1,COLOR1);
    textureImage.setPixel(1,1,COLOR2);

    //Debug pattern
    /*textureImage.setPixel(0,0,0x80ff0000);
    textureImage.setPixel(1,0,0x8000ff00);
    textureImage.setPixel(0,1,0x800000ff);
    textureImage.setPixel(1,1,0x80ffff00);*/

    QOpenGLTexture *texture = new QOpenGLTexture(textureImage,QOpenGLTexture::DontGenerateMipMaps);
    texture->setMagnificationFilter(QOpenGLTexture::Nearest);
    texture->setMinificationFilter(QOpenGLTexture::Nearest);
    texture->setWrapMode(QOpenGLTexture::Repeat);

    setTexture(texture);

    //Set rendering configuration
    RenderingConfig & wireFrameRndrConfPtr = getRndrConfRef(0);
    wireFrameRndrConfPtr.drawMode       = RenderingConfig::DrawMode_LineStrip;
    wireFrameRndrConfPtr.firstVertex    = INDICES_WIREFRAME_FIRST;
    wireFrameRndrConfPtr.vertexCount    = INDICES_WIREFRAME_COUNT;
    wireFrameRndrConfPtr.useIndices     = true;
    wireFrameRndrConfPtr.colorMode      = RenderingConfig::ColorMode_TextureCoord;
    wireFrameRndrConfPtr.lineWidth      = 2.0f;
    wireFrameRndrConfPtr.colorAFactor   = QVector4D(0.0f,0.0f,0.0f,0.0f);
    wireFrameRndrConfPtr.colorBFactor   = QVector4D(0.5f,0.5f,0.5f,0.2f);

    RenderingConfig & texturedRndrConfPtr = getRndrConfRef(1);
    texturedRndrConfPtr.drawMode        = RenderingConfig::DrawMode_TriangleStrip;
    texturedRndrConfPtr.vertexCount     = INDICES_TEXTURED_FIRST;
    texturedRndrConfPtr.vertexCount     = INDICES_TEXTURED_COUNT;
    texturedRndrConfPtr.useIndices      = true;
    texturedRndrConfPtr.colorMode       = RenderingConfig::ColorMode_TextureCoord;
    texturedRndrConfPtr.lineWidth       = 2.0f;
    texturedRndrConfPtr.colorAFactor    = QVector4D(1.0f,1.0f,1.0f,1.0f);
    texturedRndrConfPtr.colorBFactor    = QVector4D(0.0f,0.0f,0.0f,0.0f);

    createIndexBuffer(s_indices.size(), s_indices.data());
}

void RenderableMachineSpace::setLimit(QVector3D position)
{
    m_currPosition = position;
    if(!m_currPosition.isNull())  rebuildGeometry();
}

void RenderableMachineSpace::rebuildGeometry()
{
    // boundaries of the machine space
    const float xp = (m_currPosition.x() > 0.0f) ? m_currPosition.x() : 0.0f;
    const float yp = (m_currPosition.y() > 0.0f) ? m_currPosition.y() : 0.0f;
    const float zp = (m_currPosition.z() > 0.0f) ? m_currPosition.z() : 0.0f;
    const float xn = (m_currPosition.x() < 0.0f) ? m_currPosition.x() : 0.0f;
    const float yn = (m_currPosition.y() < 0.0f) ? m_currPosition.y() : 0.0f;
    const float zn = (m_currPosition.z() < 0.0f) ? m_currPosition.z() : 0.0f;


    /* Texture coordinate calculation
    //
    //                           o
    //                          / #
    //                         /   #
    //                        /     #         Texture X =
    //                       /       # <-     SqrtHalfSq(x)
    //                      / Texture #
    //                     /           #
    //                    +-------------+
    // TextureY =        #|             |\
    // SqrtHalfSq(y) -> # |             | \
    //                 #  |y   Cube     |  \
    // Texture origin o   |    face     |   o
    //                 #  |             |  #
    //                  # |             | #
    //                   #|     x       |#
    //                    #-------------#
    //                     #           #       Texture Y =
    //                      # Texture #  <-    SqrtHalfSq(x)
    //     TextureX =        #       #         + SqrdHalfSq(y)
    //     SqrtHalfSq(x)   -> #     #
    //   + SqrdHalfSq(y)       #   #
    //                          # #
    //                           o
    //Then we divide by TEXTURE_STRIPES_WIDTH_MM to set the stripe size (1 stripe = texture width)*/

    // texture coordinates
    const float tx = sqrtHalfSq(m_currPosition.x()) / TEXTURE_STRIPES_WIDTH_MM;
    const float ty = sqrtHalfSq(m_currPosition.y()) / TEXTURE_STRIPES_WIDTH_MM;
    const float tz = sqrtHalfSq(m_currPosition.z()) / TEXTURE_STRIPES_WIDTH_MM;

    //Triangles geometry
    RenderableObject::VertexData machineSpaceCube[] =
    {
        //Space coord ,     Texture coord
        //Bottom
        { {xn, yn, zn},     {ty     ,00     ,0,0 } },
        { {xn, yp, zn},     {00     ,ty     ,0,0 } },
        { {xp, yn, zn},     {tx+ty  ,tx     ,0,0 } },
        { {xp, yp, zn},     {tx     ,tx+ty  ,0,0 } },

        //Top
        { {xn, yp, zp},     {ty     ,00     ,0,0 } },
        { {xn, yn, zp},     {00     ,ty     ,0,0 } },
        { {xp, yp, zp},     {tx+ty  ,tx     ,0,0 } },
        { {xp, yn, zp},     {tx     ,tx+ty  ,0,0 } },

        //Back
        { {xn, yp, zn},     {tz     ,00     ,0,0 } },
        { {xn, yp, zp},     {00     ,tz     ,0,0 } },
        { {xp, yp, zn},     {tx+tz  ,tx     ,0,0 } },
        { {xp, yp, zp},     {tx     ,tx+tz  ,0,0 } },

        //Front
        { {xn, yn, zp},     {tz     ,00     ,0,0 } },
        { {xn, yn, zn},     {00     ,tz     ,0,0 } },
        { {xp, yn, zp},     {tx+tz  ,tx     ,0,0 } },
        { {xp, yn, zn},     {tx     ,tx+tz  ,0,0 } },

        //Left
        { {xn, yn, zn},     {tz     ,00     ,0,0 } },
        { {xn, yn, zp},     {00     ,tz     ,0,0 } },
        { {xn, yp, zn},     {ty+tz  ,ty     ,0,0 } },
        { {xn, yp, zp},     {ty     ,ty+tz  ,0,0 } },

        //Right
        { {xp, yn, zp},     {tz     ,00     ,0,0 } },
        { {xp, yn, zn},     {00     ,tz     ,0,0 } },
        { {xp, yp, zp},     {ty+tz  ,ty     ,0,0 } },
        { {xp, yp, zn},     {ty     ,ty+tz  ,0,0 } },
    };

    //Already allocated
    GLsizei machineSpaceVertexCount =
            sizeof(machineSpaceCube) / sizeof(machineSpaceCube[0]);
    createVertexBuffer(machineSpaceVertexCount, machineSpaceCube);
}

float RenderableMachineSpace::sqrtHalfSq(float a)
{
    return qSqrt(qPow(a,2.0) / 2.0);
}


void RenderableMachineSpace::render(QOpenGLShaderProgram *program)
{
    if(!m_currPosition.isNull()) RenderableObject::render(program);
}

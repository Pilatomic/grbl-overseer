#ifndef RENDEREDOBJECT_H
#define RENDEREDOBJECT_H

#include "renderingconfig.h"

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QVector3D>
#include <QVector>

class QOpenGLTexture;
class QOpenGLBuffer;

class RenderableObject : protected QOpenGLFunctions
{
public:
    struct VertexData
    {
        QVector3D position;
        QVector4D colorInfo;
    };

    explicit RenderableObject(int rndrConfCount, int matricesCount);
    virtual ~RenderableObject();

    virtual void render(QOpenGLShaderProgram *program);

protected:

    void createVertexBuffer(int verticeCount, const VertexData *data = nullptr);
    void writeVertexBuffer(quint32 index, GLsizei size, const void *data);

    void createIndexBuffer(int verticeCount, const GLushort *data = nullptr);
    void writeIndexBuffer(quint32 index, GLsizei size, const void *data);

    void performRendering(QOpenGLShaderProgram *program, int configIndex = 0);

    static QVector4D colorToVector4D(QColor color);

    void setTexture(QOpenGLTexture *texture);

    static const QMatrix4x4 generateMMatrix(QVector3D position,
                                            QQuaternion rotation = QQuaternion(),
                                            float scale = 1.0f);

    RenderingConfig &getRndrConfRef(int i);
    QMatrix4x4 &getMMatrixRef(int i);

private:
    QVector<RenderingConfig> m_renderingConfigs;
    QVector<QMatrix4x4> m_mMatrices;

    //All those can be changed to vectors if required
    QOpenGLTexture *m_texture;
    QOpenGLBuffer *m_vertexBuffer;
    QOpenGLBuffer *m_indexBuffer;
};





#endif // RENDEREDOBJECT_H

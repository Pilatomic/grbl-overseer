#ifndef RENDERABLEJOBHIGHLIGHTER_H
#define RENDERABLEJOBHIGHLIGHTER_H

#include "renderableobject.h"

class RenderableJob;

class RenderableJobHighlighter : public RenderableObject
{
public:
    RenderableJobHighlighter();

    void render(QOpenGLShaderProgram *program) override;

    void setHighlightedJob(RenderableJob* job);
    RenderableJob *getSelectedJob() const;

    void updateOffset();

    void setUseImperialUnits(bool use);

private:
    enum Axis_Enum{Axis_X = 0, Axis_Y, Axis_Z};

    QString getLengthString(float length);

    void regenerate();
    void rebuildTexture(QVector3D *textsAspectRatio); //set the aspect ratio of each text
    void rebuildGeometry(QVector3D textsAspectRatio);

    QVector<VertexData> buildDimensionVertices(float leftCoord, float rightCoord,
                                               float baseCoord, float thirdAxisPos,
                                               QVector3D textsAspectRatio,
                                               Axis_Enum axis);

    //Project coordinates for specififec axis dimension
    QVector3D vec3Axis(float x, float y, float z, Axis_Enum axis);

    RenderableJob* m_jobToHighlight;

    bool m_usesImperialUnits;

    static const QVector<GLushort> s_indices;
};

#endif // RENDERABLEJOBHIGHLIGHTER_H

#include "visualizeritem.h"

#include <QDebug>
#include <QtMath>
#include <QScreen>
#include <QTouchEvent>

#define ZOOM_MIN    0.01f
#define ZOOM_MAX    1000.0f

VisualizerItem::VisualizerItem(QQuickItem *parent) :
    QQuickFramebufferObject(parent)
{
    m_quality = 1;
    m_machineSpace.homingPositiveCoord = false;
    m_machineSpace.homingState = GrblBoard::Homing_Invalid;

    setAcceptedMouseButtons(Qt::AllButtons);
    resetView();

//    connect(this, &VisualizerItem::windowChanged,
//            this, &VisualizerItem::onWindowChanged);

    connect(&m_touchPointsManager, &TouchPointsManager::panned,
            this, &VisualizerItem::rotateCameraFromScreenVector);

    connect(&m_touchPointsManager, &TouchPointsManager::pinchScaled,
            this, &VisualizerItem::applyZoomRatio);

    connect(&m_touchPointsManager, &TouchPointsManager::pinchMoved,
            this, &VisualizerItem::translateFromScreenVector);

    connect(&m_touchPointsManager, &TouchPointsManager::pinchRotated,
            this, &VisualizerItem::rotateScreen);

    connect(&m_touchPointsManager, &TouchPointsManager::doublePress,
            this, &VisualizerItem::resetView);
}

VisualizerItem::~VisualizerItem()
{
    //qDebug()<< "Item destructed";
}

//################################### EVENTS ###################################

void VisualizerItem::mouseDoubleClickEvent(QMouseEvent *e){
    Q_UNUSED(e);
    resetView();
}

void VisualizerItem::mousePressEvent(QMouseEvent *e){
    m_mouseDownPosition = QVector2D(e->localPos());
}



void VisualizerItem::mouseMoveEvent(QMouseEvent *e)
{
    QVector2D diff = QVector2D(e->localPos()) - m_mouseDownPosition;
    m_mouseDownPosition = QVector2D(e->localPos());

    if(e->buttons() == Qt::LeftButton ){
        rotateCameraFromScreenVector(diff);
    }

    if(e->buttons() == Qt::RightButton ){
        translateFromScreenVector(diff);
    }

}

void VisualizerItem::wheelEvent(QWheelEvent *e){
    float wheelScrollDistance =(e->angleDelta().y()) / 100.0f;
    applyZoomRatio(qPow(1.2f,-wheelScrollDistance));
}

void VisualizerItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickFramebufferObject::geometryChanged(newGeometry,oldGeometry);
    if(newGeometry != oldGeometry)
    {
        m_viewData.viewPortSize = QVector2D(newGeometry.width(), newGeometry.height());
        update();
    }
}

void VisualizerItem::touchEvent(QTouchEvent *event)
{
    m_touchPointsManager.processEvent(event);
}



//########################## SIGNALS FROM JOB MANAGER ##########################

void VisualizerItem::onJobAdded(GCodeJob *job)
{
    m_eventQueue.append(VisualizerEvent::createJobCreatedEvent(job));
    update();
}

void VisualizerItem::onJobRemoved(GCodeJob *job)
{
    m_eventQueue.append(VisualizerEvent::createJobDeletedEvent(job));
    update();
}

void VisualizerItem::onJobLineStatusChanged(GCodeJob *job, int line, GCodeJobLine::Status lineStatus)
{
    if(!m_eventQueue.isEmpty())
    {
        // To avoir creating 1 event for each line, when all lines changes to same state
        // ( like when job in reset ) we compact all events in one same events :
        // int1 = first line
        // int2 = last line
        VisualizerEvent & lastEventInQueue = m_eventQueue.last();
        if(lastEventInQueue.getType() == VisualizerEvent::JobLineStatusChanged
        && lastEventInQueue.getLineStatus() == lineStatus
        && lastEventInQueue.getInt2() == (line - 1))
        {
            lastEventInQueue.setInt2(line);
            update();
            return;
        }
    }

    m_eventQueue.append(VisualizerEvent::createJobLineStatusChangedEvent(job, line, lineStatus));
    update();
}

void VisualizerItem::onJobPositionChanged(GCodeJob *job, QVector3D position)
{
    m_eventQueue.append(VisualizerEvent::createJobPositionChangedEvent(job, position));
    update();
}

void VisualizerItem::onJobColorChanged(GCodeJob *job, QColor color)
{
    m_eventQueue.append(VisualizerEvent::createJobColorChangedEvent(job, color));
    update();
}

void VisualizerItem::onSelectedJobChanged(GCodeJob *selectedJob)
{
    m_eventQueue.append(VisualizerEvent::createSelectedJobChangedEvent(selectedJob));
    update();
}

void VisualizerItem::onMachinePositionChanged(QVector3D position)
{
    m_eventQueue.append(VisualizerEvent::createMachinePositionChangedEvent(position));
    update();
}

void VisualizerItem::onMachineSpaceChanged(QVector3D machineSpace)
{
    m_machineSpace.limitPosition = machineSpace;
    updateMachineSpace();
}

void VisualizerItem::onHomingStateChanged(GrblBoard::HomingState homingState)
{
    m_machineSpace.homingState = homingState;
    updateMachineSpace();
}

void VisualizerItem::onBuildOptionsChanged(GrblBuildInfosIface::BuildOptions options)
{
    m_machineSpace.homingPositiveCoord = options.testFlag(GrblBuildInfosIface::Option_HomingForceOrigin);
    //We do not need to update machine space, this flag CAN NOT change while machine space is  displayed
}

void VisualizerItem::onAccessoryStateChanged(GrblStatusIface::AccessoryFlags accessories)
{
    int spindleDirection = 0;
    if(accessories.testFlag(GrblStatusIface::Accessory_Spindle_CW))
        spindleDirection += 1;
    if(accessories.testFlag(GrblStatusIface::Accessory_Spindle_CCW))
        spindleDirection -= 1;

    m_eventQueue.append(VisualizerEvent::createSpindleRotationChangedEvent(spindleDirection));
    update();
}

void VisualizerItem::onReportImperialUnitsChanged(bool imperialUnitsEnabled)
{
    m_eventQueue.append(VisualizerEvent::createReportImperialUnitChangedEvent(imperialUnitsEnabled));
    update();
}

/*
void VisualizerItem::onWindowChanged(QQuickWindow *)
{

}*/

void VisualizerItem::updateMachineSpace()
{
    m_eventQueue.append(VisualizerEvent::createMachineSpaceChangedEvent(getMachineSpaceLimit()));
    update();
}

QVector3D VisualizerItem::getMachineSpaceLimit()
{
    return (m_machineSpace.homingState == GrblBoard::Homing_Valid)
            ? (m_machineSpace.limitPosition * (m_machineSpace.homingPositiveCoord ? 1.0f : -1.0f))
            : QVector3D();
}

void VisualizerItem::showCompleteMachineSpace()
{
    QVector3D machineSpaceLimit = getMachineSpaceLimit();

    if(!machineSpaceLimit.isNull())
    {
        m_viewData.translation = machineSpaceLimit / -2.0f;

        float zoomFactor = qMin((machineSpaceLimit.x() / m_viewData.viewPortSize.x()),
                                (machineSpaceLimit.y() / m_viewData.viewPortSize.y()));
        setZoomLevel(qAbs(zoomFactor) * 1.05f); //Margin
    }
}

//########################### RENDERER INTERFACE ###############################

bool VisualizerItem::hasEventsAvailable()
{
    //if(!m_eventQueue.isEmpty()) qDebug()  << m_eventQueue.size() << "events ";
    return !m_eventQueue.isEmpty();
}

VisualizerEvent VisualizerItem::takeEvent()
{
    return m_eventQueue.dequeue();
}

int VisualizerItem::getQuality() const
{
    return m_quality;
}

void VisualizerItem::setQuality(int q)
{
    m_quality = q;
}

QQuickFramebufferObject::Renderer *VisualizerItem::createRenderer() const
{
    //qDebug()<< "Item asked to created Renderer";
    return new VisualizerItemRenderer(m_quality);
}

//############################ PUBLIC METHODS ##################################


void VisualizerItem::translateFromScreenVector(QVector2D translatedVector)
{
    QVector3D sceneSpaceVector = translatedVector * QVector2D(1.0f,-1.0f) * m_viewData.zoomLevel;  //Invert Y axis because of openGL inverted-Y oddity;
    m_viewData.translation += m_viewData.rotation.inverted().rotatedVector(sceneSpaceVector);
    update();
}

void VisualizerItem::rotateScreen(float angle)
{
    //This is the rotation axis the user want, perpendicular to the screen
    QVector3D rotationAxis = QVector3D(0.0f, 0.0f, 1.0f);

    //Now we apply the rotation
    m_viewData.rotation = QQuaternion::fromAxisAndAngle(rotationAxis.normalized(), angle) * m_viewData.rotation;
    update();
}

void VisualizerItem::rotateCameraFromScreenVector(QVector2D screenVector)
{
    //This is the rotation axis the user want, perpendicular to the mouse position difference vector
    QVector3D rotationAxis = QVector3D(screenVector.y(), screenVector.x(),0.0f);

    //Now we apply the rotation
    m_viewData.rotation = QQuaternion::fromAxisAndAngle(rotationAxis.normalized(), screenVector.length()) * m_viewData.rotation;
    update();
}

void VisualizerItem::setZoomLevel(float zoom)
{
    m_viewData.zoomLevel = qBound(ZOOM_MIN, zoom, ZOOM_MAX);
    update();
}

void VisualizerItem::applyZoomRatio(float ratio)
{
    setZoomLevel(m_viewData.zoomLevel * ratio);
}

void VisualizerItem::resetView()
{
    m_viewData.viewPortSize = QVector2D(width(), height());
    m_viewData.rotation = QQuaternion();
    m_viewData.translation =  QVector3D();
    m_viewData.zoomLevel = 1.0f;

    showCompleteMachineSpace();

    update();
}

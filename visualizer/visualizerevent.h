#ifndef VISUALIZEREVENT_H
#define VISUALIZEREVENT_H

#include "gcode/gcodejobline.h"

#include <QVector3D>
#include <QColor>

class GCodeJob;

class VisualizerEvent
{
public:
    enum Type { Unknown,
                JobCreated, JobDeleted, JobLineStatusChanged, JobPositionChanged, JobColorChanged,
                SelectedJobChanged,
                MachinePositionChanged,
                MachineSpaceChanged,
                SpindleRotationChanged,
                ReportImperialUnitsChanged
              };

    VisualizerEvent();

    VisualizerEvent::Type getType() const;
    const GCodeJob *getJobPointer() const;
    GCodeJobLine::Status getLineStatus() const;
    int getInt1() const;
    void setInt2(int int2);
    int getInt2() const;
    QVector3D getPosition() const;
    QColor getColor() const;

    static VisualizerEvent createJobCreatedEvent(const GCodeJob *jobPointer);
    static VisualizerEvent createJobDeletedEvent(const GCodeJob *jobPointer);
    static VisualizerEvent createJobLineStatusChangedEvent(const GCodeJob *jobPointer, int line, GCodeJobLine::Status status);
    static VisualizerEvent createJobPositionChangedEvent(const GCodeJob *jobPointer, QVector3D position);
    static VisualizerEvent createJobColorChangedEvent(const GCodeJob *jobPointer, QColor color);

    static VisualizerEvent createSelectedJobChangedEvent(const GCodeJob *jobPointer);

    static VisualizerEvent createMachinePositionChangedEvent(QVector3D position);

    static VisualizerEvent createMachineSpaceChangedEvent(QVector3D position);

    static VisualizerEvent createSpindleRotationChangedEvent(int direction);

    static VisualizerEvent createReportImperialUnitChangedEvent(bool imperialUnits);

private:
    Type m_type;
    const GCodeJob* m_jobPointer;

    //Consider using union for those ?
    int m_int1;
    int m_int2;
    GCodeJobLine::Status m_status;
    QVector3D m_position;
    QColor m_color;
};

#endif // VISUALIZEREVENT_H

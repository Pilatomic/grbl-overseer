import QtQuick 2.0

import QtGraphicalEffects 1.0


Item {
    property bool active: false
    property string imageSource ;
    property color colorOff: "black"
    property color colorOn: "white"

    Image {
        id: icon
        source: imageSource
        anchors.fill: parent
        sourceSize.height: height   //Appears to make image look better
        sourceSize.width: width     //Appears to make image look better
        fillMode: Image.PreserveAspectFit
        smooth: true
        visible: false
    }

    ColorOverlay {
        id:iconOverlay
        anchors.fill: icon
        source: icon
        color: getOverlayColor()
        visible: true

        function getOverlayColor() {
            return active ? colorOn : colorOff;
        }
    }
}

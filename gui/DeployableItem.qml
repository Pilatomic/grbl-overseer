import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GCodeJob 1.0

import StyleFile 1.0

Rectangle {
    property bool deployed : ListView ? ListView.isCurrentItem : false

    property int mainHeight: 0
    property int extendedHeight: 0

    property alias mainItem: staticPane.data
    property alias extendedItem: mobilePaneContent.data

    id:deployableItem

    height: staticPane.height + mobilePane.height

    color: model.index % 2 ? Style.deployableItem.evenColor: Style.deployableItem.oddColor

    states: [
        State {
            when: deployed
            PropertyChanges { target: mobilePane ; height: extendedHeight ; visible: true}
            PropertyChanges { target: deployableItem; color: Style.deployableItem.deployedColor}
        }
    ]

    transitions: Transition {
        NumberAnimation {
            target: mobilePane
            duration: potatoGpu ? 0 : 300
            properties: "height"
            easing.type: Easing.InOutQuad
        }
        ColorAnimation {
            target: deployableItem
            duration: potatoGpu ? 0 : 300
            property:"color"
            easing.type: Easing.InOutQuad
        }
    }

    MouseArea {
        onClicked: {
            if(typeof model !== "undefined") {
                deployableItem.ListView.view.currentIndex = deployed ? -1 : model.index;
            }
            else {
                deployed = !deployed;
            }
        }

        anchors.fill: parent
    }

    Rectangle {
        id: staticPane
        height: mainHeight
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: "transparent"
    }

    Flickable {
        id: mobilePane
        anchors.top: staticPane.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        height: 0

        contentWidth: width
        contentHeight: extendedHeight

        visible: false

        clip: true

        Rectangle {
            id: mobilePaneContent

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top

            height: extendedHeight;

            color: "transparent"
        }
    }
}


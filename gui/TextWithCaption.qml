import QtQuick 2.0

Item {
    property alias valueText: value
    property alias captionText: caption

    Text{
        id:caption

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: value.top
        anchors.margins: parent.height * 0.05
        anchors.bottomMargin: 0

        fontSizeMode: Text.Fit
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        minimumPixelSize: 2
        font.pixelSize: 72
        font.weight: Font.Bold
        //font.capitalization: Font.AllUppercase
        font.bold: false
    }

    Text{
        id:value

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: parent.height * 0.05
        anchors.topMargin: 0
        height: parent.height * 0.6

        fontSizeMode: Text.Fit
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        minimumPixelSize: 8
        font.pixelSize: 72
        font.weight: Font.Bold
        font.capitalization: Font.AllUppercase
        font.bold: false
    }
}

import QtQuick 2.0
import QtQuick.Layouts 1.2


RowLayout {
    anchors.fill: parent
    anchors.leftMargin:     parent.width * 0.02
    anchors.rightMargin:    parent.width * 0.02

    spacing: 0

    readonly property real sizeUnit: width / 8

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/LimX"
        active: grblStatus.activeInputPins & GrblStatus.Input_X_Limit
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/LimY"
        active: grblStatus.activeInputPins & GrblStatus.Input_Y_Limit
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/LimZ"
        active: grblStatus.activeInputPins & GrblStatus.Input_Z_Limit
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/Probe"
        active: grblStatus.activeInputPins & GrblStatus.Input_Probe
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/Door"
        active: grblStatus.activeInputPins & GrblStatus.Input_Door
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/Start"
        active: grblStatus.activeInputPins & GrblStatus.Input_CycleStart
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/Hold"
        active: grblStatus.activeInputPins & GrblStatus.Input_Hold
    }

    Indicator {
        Layout.preferredHeight: parent.sizeUnit
        Layout.preferredWidth: parent.sizeUnit
        imageSource: "qrc:/icons/io/Reset"
        active: grblStatus.activeInputPins & GrblStatus.Input_SoftReset
    }
}

import QtQuick 2.0

import StyleFile 1.0

Rectangle {
    property Flickable target: parent

    anchors.right: parent.right

    width: target.width * Style.scrollBar.widthFactor

    visible: target.visibleArea.heightRatio !== 1.0

    height: target.visibleArea.heightRatio * target.height

    y: target.visibleArea.yPosition * target.height

    color: Style.scrollBar.color
}

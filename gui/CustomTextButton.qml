import QtQuick 2.0

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import StyleFile 1.0

Rectangle {
    property string imageSource ;
    property color overlayColorIdle: Style.customButton.defaultColor;
    property color overlayColorDisabled: Qt.darker(overlayColorIdle);
    property color overlayColorPressed : Qt.darker(overlayColorIdle);
    property color overlayColorTabActive : overlayColorPressed;
    property color backgroundColorIdle: "transparent"
    property color backgroundColorClicked: "transparent";
    property color backgroundColorEngaged: backgroundColorClicked;
    property color textColorIdle: Style.customButton.defaultColor;
    property color textColorDisabled: Qt.darker(textColorIdle);
    property bool engaged: false
    property alias text : captionText.text
    signal clicked()

    id:customTextButton

    height: width

    color: engaged ? (backgroundColorEngaged) :
                        ( mouseArea.pressed ? backgroundColorClicked : backgroundColorIdle );

    MouseArea {
        id:mouseArea
        anchors.fill: parent
        onClicked: {
            if(customTextButton.enabled){
                parent.clicked()
            }
        }
    }

    Text {
        id:captionText
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: parent.height
        color: getTextColor()
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height * 0.5
        font.bold: true

        function getTextColor() {
            if(customTextButton.enabled === true)
                return textColorIdle
            else
                return textColorDisabled
        }
    }
}

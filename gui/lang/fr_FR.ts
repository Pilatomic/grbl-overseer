<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name></name>
    <message id="dialog-button-ok">
        <location filename="../CustomDialog.qml" line="239"/>
        <location filename="../LocalizedStrings.qml" line="6"/>
        <source>Ok</source>
        <extracomment>The Ok button in generic dialogs</extracomment>
        <translation>Ok</translation>
    </message>
    <message id="dialog-button-yes">
        <location filename="../CustomDialog.qml" line="316"/>
        <location filename="../LocalizedStrings.qml" line="10"/>
        <source>Yes</source>
        <extracomment>The Yes button in generic dialogs</extracomment>
        <translation>Oui</translation>
    </message>
    <message id="dialog-button-no">
        <location filename="../CustomDialog.qml" line="330"/>
        <location filename="../LocalizedStrings.qml" line="14"/>
        <source>No</source>
        <extracomment>The No button in generic dialogs</extracomment>
        <translation>Non</translation>
    </message>
    <message id="dialog-button-abort">
        <location filename="../CustomDialog.qml" line="344"/>
        <location filename="../LocalizedStrings.qml" line="18"/>
        <source>Abort</source>
        <extracomment>The Abort button in generic dialogs</extracomment>
        <translation>Abandon</translation>
    </message>
    <message id="grbl-settings-value-placeholder">
        <location filename="../GRBLSettingsDelegate.qml" line="124"/>
        <location filename="../GRBLSettingsDelegate.qml" line="154"/>
        <location filename="../LocalizedStrings.qml" line="22"/>
        <source>Value</source>
        <extracomment>The placeholder in GRBL config value field</extracomment>
        <translation>Valeur</translation>
    </message>
    <message id="grbl-settings-value-bool-no">
        <location filename="../GRBLSettingsDelegate.qml" line="145"/>
        <location filename="../LocalizedStrings.qml" line="30"/>
        <source>No</source>
        <extracomment>The label for &quot;false&quot; in GRBL config bool field</extracomment>
        <translation>Non</translation>
    </message>
    <message id="grbl-settings-value-bool-yes">
        <location filename="../GRBLSettingsDelegate.qml" line="145"/>
        <location filename="../LocalizedStrings.qml" line="26"/>
        <source>Yes</source>
        <extracomment>The label for &quot;true&quot; in GRBL config bool field</extracomment>
        <translation>Oui</translation>
    </message>
    <message id="label-lines-count">
        <location filename="../JobDelegate.qml" line="61"/>
        <location filename="../LocalizedStrings.qml" line="34"/>
        <source>%1 lines</source>
        <extracomment>The line count in a task</extracomment>
        <translation>%1 lignes</translation>
    </message>
    <message id="job-button-set-origin-here">
        <location filename="../JobDelegate.qml" line="162"/>
        <location filename="../LocalizedStrings.qml" line="38"/>
        <source>Set origin here</source>
        <extracomment>The label of the &quot;Set origin here&quot; job button</extracomment>
        <translation>Définir l&apos;origine ici</translation>
    </message>
    <message id="job-button-randomise-color">
        <location filename="../JobDelegate.qml" line="192"/>
        <location filename="../LocalizedStrings.qml" line="42"/>
        <source>Change color</source>
        <extracomment>The label of the &quot;Change color&quot; job button</extracomment>
        <translation>Chgr. la couleur</translation>
    </message>
    <message id="job-button-delete">
        <location filename="../JobDelegate.qml" line="220"/>
        <location filename="../LocalizedStrings.qml" line="46"/>
        <source>Delete job</source>
        <extracomment>The label of the &quot;Delete job&quot; job button</extracomment>
        <translation>Supprimer la tâche</translation>
    </message>
    <message id="job-label-origin">
        <location filename="../JobDelegate.qml" line="248"/>
        <source>Job origin :</source>
        <extracomment>The label of the job origin field group</extracomment>
        <translation>Origine :</translation>
    </message>
    <message id="label-unit-mm">
        <location filename="../JobDelegate.qml" line="250"/>
        <location filename="../LocalizedStrings.qml" line="297"/>
        <location filename="../TopBar.qml" line="107"/>
        <source>(mm)</source>
        <extracomment>Label of the milimeter unit</extracomment>
        <translation>(mm)</translation>
    </message>
    <message id="axis-label-x">
        <location filename="../JobDelegate.qml" line="267"/>
        <location filename="../LocalizedStrings.qml" line="50"/>
        <source>X</source>
        <extracomment>The label of the X axis</extracomment>
        <translation>X</translation>
    </message>
    <message id="axis-label-y">
        <location filename="../JobDelegate.qml" line="275"/>
        <location filename="../LocalizedStrings.qml" line="54"/>
        <source>Y</source>
        <extracomment>The label of the Y axis</extracomment>
        <translation>Y</translation>
    </message>
    <message id="axis-label-z">
        <location filename="../JobDelegate.qml" line="283"/>
        <location filename="../LocalizedStrings.qml" line="58"/>
        <source>Z</source>
        <extracomment>The label of the Z axis</extracomment>
        <translation>Z</translation>
    </message>
    <message id="axis-label-y-positive">
        <location filename="../JogControls.qml" line="132"/>
        <location filename="../LocalizedStrings.qml" line="70"/>
        <source>Y+</source>
        <extracomment>The label of the Y+ axis</extracomment>
        <translation>Y+</translation>
    </message>
    <message id="axis-label-y-negative">
        <location filename="../JogControls.qml" line="147"/>
        <location filename="../LocalizedStrings.qml" line="74"/>
        <source>Y-</source>
        <extracomment>The label of the Y- axis</extracomment>
        <translation>Y-</translation>
    </message>
    <message id="axis-label-x-negative">
        <location filename="../JogControls.qml" line="162"/>
        <location filename="../LocalizedStrings.qml" line="66"/>
        <source>X-</source>
        <extracomment>The label of the X- axis</extracomment>
        <translation>X-</translation>
    </message>
    <message id="axis-label-x-positive">
        <location filename="../JogControls.qml" line="177"/>
        <location filename="../LocalizedStrings.qml" line="62"/>
        <source>X+</source>
        <extracomment>The label of the X+ axis</extracomment>
        <translation>X+</translation>
    </message>
    <message id="axis-label-z-positive">
        <location filename="../JogControls.qml" line="275"/>
        <location filename="../LocalizedStrings.qml" line="78"/>
        <source>Z+</source>
        <extracomment>The label of the Z+ axis</extracomment>
        <translation>Z+</translation>
    </message>
    <message id="axis-label-z-negative">
        <location filename="../JogControls.qml" line="288"/>
        <location filename="../LocalizedStrings.qml" line="82"/>
        <source>Z-</source>
        <extracomment>The label of the Z- axis</extracomment>
        <translation>Z-</translation>
    </message>
    <message id="label-jog-group">
        <location filename="../LocalizedStrings.qml" line="86"/>
        <location filename="../PanelControls.qml" line="32"/>
        <source>Jog</source>
        <extracomment>The label of the jog control group title</extracomment>
        <translation>Déplacements</translation>
    </message>
    <message id="label-manual-control-group">
        <location filename="../LocalizedStrings.qml" line="90"/>
        <location filename="../PanelControls.qml" line="55"/>
        <source>Manual Commands</source>
        <extracomment>The manual control group title</extracomment>
        <translation>Contrôle manuel</translation>
    </message>
    <message id="button-homing">
        <location filename="../LocalizedStrings.qml" line="93"/>
        <location filename="../PanelControls.qml" line="68"/>
        <source>Run homing</source>
        <translation>Ret. origine machine</translation>
    </message>
    <message id="button-alarm-reset">
        <location filename="../LocalizedStrings.qml" line="96"/>
        <location filename="../PanelControls.qml" line="92"/>
        <source>Kill alarm</source>
        <translation>Déact. l’arrêt d&apos;urgence</translation>
    </message>
    <message id="label-overrides-group">
        <location filename="../LocalizedStrings.qml" line="99"/>
        <location filename="../PanelControls.qml" line="110"/>
        <source>Overrides</source>
        <translation>Modificateurs</translation>
    </message>
    <message id="label-feed-rate">
        <location filename="../LocalizedStrings.qml" line="102"/>
        <location filename="../PanelControls.qml" line="129"/>
        <source>Feed motions</source>
        <translation>Vitesse d&apos;usinage</translation>
    </message>
    <message id="label-move-rate">
        <location filename="../LocalizedStrings.qml" line="105"/>
        <location filename="../PanelControls.qml" line="170"/>
        <source>Rapid motions</source>
        <translation>Vitesse de déplacement</translation>
    </message>
    <message id="label-spin-rate">
        <location filename="../LocalizedStrings.qml" line="108"/>
        <location filename="../PanelControls.qml" line="199"/>
        <source>Spindle RPM</source>
        <translation>Vitesse de rot. de l&apos;outil</translation>
    </message>
    <message id="button-load-more-jobs">
        <location filename="../LocalizedStrings.qml" line="111"/>
        <location filename="../PanelJobs.qml" line="36"/>
        <source>Load more G-Code jobs</source>
        <translation>Charger plus de tâches</translation>
    </message>
    <message id="button-load-job">
        <location filename="../LocalizedStrings.qml" line="114"/>
        <location filename="../PanelJobs.qml" line="36"/>
        <source>Load G-Code jobs</source>
        <translation>Charger des tâches</translation>
    </message>
    <message id="dialog-select-job-file-title">
        <location filename="../LocalizedStrings.qml" line="306"/>
        <location filename="../PanelJobs.qml" line="73"/>
        <source>Please choose a file</source>
        <extracomment>Title of the job file dialog</extracomment>
        <translation>Veuillez choisir un fichier</translation>
    </message>
    <message id="label-monitor-plan-buffer-error">
        <location filename="../LocalizedStrings.qml" line="119"/>
        <location filename="../PanelMonitor.qml" line="63"/>
        <source>Plan Buf : Err</source>
        <translation>Erreur tampon planification</translation>
    </message>
    <message id="label-monitor-plan-buffer">
        <location filename="../LocalizedStrings.qml" line="122"/>
        <location filename="../PanelMonitor.qml" line="65"/>
        <source>Plan Buf : </source>
        <translation>Tampon planification : </translation>
    </message>
    <message id="label-monitor-char-buffer-error">
        <location filename="../LocalizedStrings.qml" line="125"/>
        <location filename="../PanelMonitor.qml" line="100"/>
        <source>Char Buf : Err</source>
        <translation>Erreur tampon caractères</translation>
    </message>
    <message id="label-monitor-char-buffer">
        <location filename="../LocalizedStrings.qml" line="128"/>
        <location filename="../PanelMonitor.qml" line="102"/>
        <source>Char Buf : </source>
        <translation>Tampon caractères : </translation>
    </message>
    <message id="input-field-manual-command">
        <location filename="../LocalizedStrings.qml" line="131"/>
        <location filename="../PanelMonitor.qml" line="154"/>
        <source>Type manual commands here</source>
        <translation>Entrez les commandes  G-CODE ici</translation>
    </message>
    <message id="label-serial-link-group">
        <location filename="../LocalizedStrings.qml" line="134"/>
        <location filename="../PanelSettings.qml" line="13"/>
        <source>Serial Link</source>
        <translation>Liaison avec la carte de contrôle</translation>
    </message>
    <message id="button-serial-link-disconnect">
        <location filename="../LocalizedStrings.qml" line="137"/>
        <location filename="../PanelSettings.qml" line="66"/>
        <source>Disconnect</source>
        <translation>Déconnexion</translation>
    </message>
    <message id="button-serial-link-connect">
        <location filename="../LocalizedStrings.qml" line="140"/>
        <location filename="../PanelSettings.qml" line="66"/>
        <source>Connect</source>
        <translation>Connexion</translation>
    </message>
    <message id="label-grbl-conf-group">
        <location filename="../LocalizedStrings.qml" line="143"/>
        <location filename="../PanelSettings.qml" line="89"/>
        <source>GRBL Configuration</source>
        <translation>Configuration de GRBL</translation>
    </message>
    <message id="label-settings-machine-config-not-available">
        <location filename="../LocalizedStrings.qml" line="146"/>
        <location filename="../PanelSettings.qml" line="121"/>
        <source>Machine
configuration
not available</source>
        <translation>Configuration de
la carte de contrôle
non disponible</translation>
    </message>
    <message id="label-coordinates-work">
        <location filename="../LocalizedStrings.qml" line="149"/>
        <location filename="../TopBar.qml" line="108"/>
        <source>Work.</source>
        <translation>Work.</translation>
    </message>
    <message id="label-coordinates-machine">
        <location filename="../LocalizedStrings.qml" line="152"/>
        <location filename="../TopBar.qml" line="108"/>
        <source>Mach.</source>
        <translation>Mach.</translation>
    </message>
    <message id="label-status-title">
        <location filename="../LocalizedStrings.qml" line="157"/>
        <location filename="../TopBar.qml" line="123"/>
        <source>Grbl State</source>
        <translation>État</translation>
    </message>
    <message id="label-grbl-status-idle">
        <location filename="../LocalizedStrings.qml" line="160"/>
        <location filename="../TopBar.qml" line="130"/>
        <source>Idle</source>
        <translation>En attente</translation>
    </message>
    <message id="label-grbl-status-running">
        <location filename="../LocalizedStrings.qml" line="163"/>
        <location filename="../TopBar.qml" line="132"/>
        <source>Running</source>
        <translation>Usinage</translation>
    </message>
    <message id="label-grbl-status-paused ">
        <location filename="../LocalizedStrings.qml" line="166"/>
        <location filename="../TopBar.qml" line="134"/>
        <source>Paused</source>
        <translation>Pause</translation>
    </message>
    <message id="label-grbl-status-jogging">
        <location filename="../LocalizedStrings.qml" line="169"/>
        <location filename="../TopBar.qml" line="136"/>
        <source>Jogging</source>
        <translation>Déplacement</translation>
    </message>
    <message id="label-grbl-status-door">
        <location filename="../LocalizedStrings.qml" line="172"/>
        <location filename="../TopBar.qml" line="138"/>
        <source>Door</source>
        <translation>Porte ouverte</translation>
    </message>
    <message id="label-grbl-status-homing">
        <location filename="../LocalizedStrings.qml" line="175"/>
        <location filename="../TopBar.qml" line="140"/>
        <source>Homing</source>
        <translation>Retour origine</translation>
    </message>
    <message id="label-grbl-status-alarm">
        <location filename="../LocalizedStrings.qml" line="178"/>
        <location filename="../TopBar.qml" line="142"/>
        <source>Alarm</source>
        <translation>Arrêt d&apos;urgence</translation>
    </message>
    <message id="label-grbl-status-simulation">
        <location filename="../LocalizedStrings.qml" line="181"/>
        <location filename="../TopBar.qml" line="144"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message id="label-grbl-status-sleeping">
        <location filename="../LocalizedStrings.qml" line="184"/>
        <location filename="../TopBar.qml" line="146"/>
        <source>Sleeping</source>
        <translation>En veille</translation>
    </message>
    <message id="label-grbl-status-unknown">
        <location filename="../LocalizedStrings.qml" line="187"/>
        <location filename="../TopBar.qml" line="148"/>
        <source>Status unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message id="label-grbl-status-disconnected">
        <location filename="../LocalizedStrings.qml" line="190"/>
        <location filename="../TopBar.qml" line="150"/>
        <source>Not connected</source>
        <translation>Hors ligne</translation>
    </message>
    <message id="tab-jobs">
        <location filename="../LocalizedStrings.qml" line="194"/>
        <location filename="../main.qml" line="150"/>
        <location filename="../main.qml" line="228"/>
        <source>Jobs</source>
        <extracomment>The label of the jobs tab</extracomment>
        <translation>Tâches</translation>
    </message>
    <message id="tab-controls">
        <location filename="../LocalizedStrings.qml" line="197"/>
        <location filename="../main.qml" line="161"/>
        <location filename="../main.qml" line="239"/>
        <source>Controls</source>
        <extracomment>The label of the controls tab</extracomment>
        <translation>Contrôles</translation>
    </message>
    <message id="tab-monitor">
        <location filename="../LocalizedStrings.qml" line="200"/>
        <location filename="../main.qml" line="172"/>
        <location filename="../main.qml" line="250"/>
        <source>Monitor</source>
        <extracomment>The label of the monitor tab</extracomment>
        <translation>Moniteur</translation>
    </message>
    <message id="tab-settings">
        <location filename="../LocalizedStrings.qml" line="203"/>
        <location filename="../main.qml" line="183"/>
        <location filename="../main.qml" line="261"/>
        <source>Settings</source>
        <extracomment>The label of the settings tab</extracomment>
        <translation>Config.</translation>
    </message>
    <message id="button-start-no-sim">
        <location filename="../LocalizedStrings.qml" line="207"/>
        <location filename="../main.qml" line="310"/>
        <source>Go
(No Sim.)</source>
        <translation>Démarrer
(sans sim.)</translation>
    </message>
    <message id="button-start">
        <location filename="../LocalizedStrings.qml" line="210"/>
        <location filename="../main.qml" line="310"/>
        <source>Go</source>
        <translation>Démarrer</translation>
    </message>
    <message id="button-resume">
        <location filename="../LocalizedStrings.qml" line="213"/>
        <location filename="../main.qml" line="325"/>
        <source>Resume</source>
        <translation>Reprendre</translation>
    </message>
    <message id="button-pause">
        <location filename="../LocalizedStrings.qml" line="216"/>
        <location filename="../main.qml" line="338"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message id="button-reset">
        <location filename="../LocalizedStrings.qml" line="219"/>
        <location filename="../main.qml" line="364"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message id="dialog-open-job-error-title">
        <location filename="../LocalizedStrings.qml" line="222"/>
        <location filename="../main.qml" line="377"/>
        <source>Error opening job</source>
        <translation>Erreur lors du chargement d&apos;une tâche</translation>
    </message>
    <message id="dialog-open-job-error-text">
        <location filename="../LocalizedStrings.qml" line="225"/>
        <location filename="../main.qml" line="378"/>
        <source>An error was encountered while trying to open a job</source>
        <translation>Une erreur s&apos;est produite lors du chargement d&apos;une tâche</translation>
    </message>
    <message id="dialog-open-job-error-info">
        <location filename="../LocalizedStrings.qml" line="228"/>
        <location filename="../main.qml" line="379"/>
        <source>Error message :</source>
        <translation>Message d&apos;erreur :</translation>
    </message>
    <message id="dialog-all-jobs-complete-title">
        <location filename="../LocalizedStrings.qml" line="231"/>
        <location filename="../main.qml" line="395"/>
        <source>All jobs completed !</source>
        <translation>Toutes les tâches sont terminées !</translation>
    </message>
    <message id="dialog-job-sim-error-title">
        <location filename="../LocalizedStrings.qml" line="234"/>
        <location filename="../main.qml" line="403"/>
        <source>Simulation completed with errors</source>
        <translation>Des erreurs se sont produites lors de la simulation</translation>
    </message>
    <message id="dialog-job-sim-error-text">
        <location filename="../LocalizedStrings.qml" line="237"/>
        <location filename="../main.qml" line="404"/>
        <source>Start production anyway ?</source>
        <translation>Démarrer la production malgré tout ?</translation>
    </message>
    <message id="dialog-job-sim-error-info">
        <location filename="../LocalizedStrings.qml" line="240"/>
        <location filename="../main.qml" line="405"/>
        <source>Errors :</source>
        <translation>Erreurs :</translation>
    </message>
    <message id="dialog-job-sim-complete-title">
        <location filename="../LocalizedStrings.qml" line="243"/>
        <location filename="../main.qml" line="416"/>
        <source>Simulation completed without errors</source>
        <translation>La simulation s&apos;est terminée sans erreur</translation>
    </message>
    <message id="dialog-job-sim-complete-text">
        <location filename="../LocalizedStrings.qml" line="246"/>
        <location filename="../main.qml" line="417"/>
        <source>Start production ?</source>
        <translation>Démarrer la production ?</translation>
    </message>
    <message id="dialog-error-during-production-title">
        <location filename="../LocalizedStrings.qml" line="249"/>
        <location filename="../main.qml" line="428"/>
        <source>Command Error</source>
        <translation>Une erreur est survenue lors de la production</translation>
    </message>
    <message id="dialog-error-during-production-text">
        <location filename="../LocalizedStrings.qml" line="252"/>
        <location filename="../main.qml" line="429"/>
        <source>Grbl just returned an error. Continue despite error ?</source>
        <translation>La carte de contrôle a retourné une erreur. Continuer malgré l&apos;erreur ?</translation>
    </message>
    <message id="dialog-error-during-production-info">
        <location filename="../LocalizedStrings.qml" line="255"/>
        <location filename="../main.qml" line="430"/>
        <source>Error message :</source>
        <translation>Erreur :</translation>
    </message>
    <message id="dialog-alarm-title">
        <location filename="../LocalizedStrings.qml" line="258"/>
        <location filename="../main.qml" line="442"/>
        <source>Alarm</source>
        <translation>Arrêt d&apos;urgence</translation>
    </message>
    <message id="dialog-alarm-text">
        <location filename="../LocalizedStrings.qml" line="261"/>
        <location filename="../main.qml" line="443"/>
        <source>Grbl is in Alarm state. Use Kill Alarm or Run homing commands in Controls tab to return to idle mode</source>
        <translation>Un arrêt d&apos;urgence a été déclenché. Veuillez le déactiver via l&apos;onglet &quot;Contrôles&quot;.</translation>
    </message>
    <message id="dialog-alarm-info">
        <location filename="../LocalizedStrings.qml" line="264"/>
        <location filename="../main.qml" line="444"/>
        <source>Alarm message : </source>
        <translation>Détails de l&apos;arrêt d&apos;urgence : </translation>
    </message>
    <message id="dialog-homing-title">
        <location filename="../LocalizedStrings.qml" line="267"/>
        <location filename="../main.qml" line="487"/>
        <source>Please wait</source>
        <translation>Veuillez patienter</translation>
    </message>
    <message id="dialog-homing-text">
        <location filename="../LocalizedStrings.qml" line="270"/>
        <location filename="../main.qml" line="488"/>
        <source>Performing homing sequence...</source>
        <translation>Retour à l&apos;origine en cours...</translation>
    </message>
    <message id="dialog-close-idle-title">
        <location filename="../LocalizedStrings.qml" line="273"/>
        <location filename="../main.qml" line="499"/>
        <source>Please confirm action</source>
        <translation>Veuillez confirmer</translation>
    </message>
    <message id="dialog-close-idle-text">
        <location filename="../LocalizedStrings.qml" line="276"/>
        <location filename="../main.qml" line="500"/>
        <source>Do you really want to quit ?</source>
        <translation>Voulez-vous vraiment quitter ?</translation>
    </message>
    <message id="dialog-close-running-title">
        <location filename="../LocalizedStrings.qml" line="279"/>
        <location filename="../main.qml" line="509"/>
        <source>Action restricted</source>
        <translation>Action restreinte</translation>
    </message>
    <message id="dialog-close-running-text">
        <location filename="../LocalizedStrings.qml" line="282"/>
        <location filename="../main.qml" line="510"/>
        <source>Can not quit while jobs are running</source>
        <translation>Vous ne pouvez pas quitter lorsqu&apos;une ou plusieurs tâches sont en cours d&apos;exécution.</translation>
    </message>
    <message id="dialog-close-running-info">
        <location filename="../LocalizedStrings.qml" line="285"/>
        <location filename="../main.qml" line="511"/>
        <source>Use &quot;Reset&quot; button to abort job</source>
        <translation>Veuillez d&apos;abord annuler les tâches en cours avec le bouton réinitialiser</translation>
    </message>
    <message id="dialog-unsupported-grbl-title">
        <location filename="../LocalizedStrings.qml" line="288"/>
        <location filename="../main.qml" line="517"/>
        <source>Unsupported GRBL version</source>
        <translation>Cette version de GRBL n&apos;est pas supportée</translation>
    </message>
    <message id="dialog-unsupported-grbl-text">
        <location filename="../LocalizedStrings.qml" line="291"/>
        <location filename="../main.qml" line="518"/>
        <source>This GRBL version is not supported, only auxiliary features will be available.
 Please upgrade to Grbl 1.1 or later</source>
        <translation>La carte de contrôle utilise une version de GRBL qui n&apos;est pas supportée. Seules les fonctionnalités auxiliaires sont disponibles.
Merci de mettre à jour votre carte vers GRBL v1.1 ou supérieur</translation>
    </message>
    <message id="label-unit-in">
        <location filename="../LocalizedStrings.qml" line="301"/>
        <location filename="../TopBar.qml" line="107"/>
        <source>(in)</source>
        <extracomment>Label of the inch unit</extracomment>
        <translation>(in)</translation>
    </message>
</context>
</TS>

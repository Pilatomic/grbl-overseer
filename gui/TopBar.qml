import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GrblStatus 1.0

import StyleFile 1.0


ProgressBar {
    id: topBar

    value: jobManager.totalProgression

    property color bgColor: Style.topbar.bgColor.defaultColorBg
    property color pgColor: Style.topbar.bgColor.defaultColorBg

    style:ProgressBarStyle {
        background: Rectangle {
            color : topBar.bgColor
        }
        progress: Rectangle {
            color : topBar.pgColor
        }
    }

    states: [
        State {
            name: "state_offline"
            when: grblStatus.currentState === GrblStatus.State_Offline
            PropertyChanges { target: topBar  ; bgColor: Style.topbar.bgColor.offlineBg }
            PropertyChanges { target: topBar  ; pgColor: Style.topbar.bgColor.offlinePg }
        },
        State {
            name: "state_unknown"
            when: grblStatus.currentState === GrblStatus.State_Unknown
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.unknownStateBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.unknownStatePg }
        },
        State {
            name: "state_sleeping"
            when: grblStatus.currentState === GrblStatus.State_Sleep
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.sleepingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.sleepingPg }
        },
        State {
            name: "state_idle"
            when: grblStatus.currentState === GrblStatus.State_Idle
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.idleBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.idlePg }
        },
        State {
            name: "state_running"
            when: grblStatus.currentState === GrblStatus.State_Run
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.runningBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.runningPg }
        },
        State {
            name: "state_jogging"
            when: grblStatus.currentState === GrblStatus.State_Jog
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.joggingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.joggingPg }
        },
        State {
            name: "state_homing"
            when: grblStatus.currentState === GrblStatus.State_Home
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.homingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.homingPg }
        },
        State {
            name: "state_door"
            when: grblStatus.currentState === GrblStatus.State_Door
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.doorBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.doorPg }
        },
        State {
            name: "state_paused"
            when: grblStatus.currentState === GrblStatus.State_Hold
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.pausedBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.pausedPg }
        },
        State {
            name: "state_simulating"
            when: grblStatus.currentState === GrblStatus.State_Check
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.simulatingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.simulatingPg }
        },
        State {
            name: "state_alarm"
            when: grblStatus.currentState === GrblStatus.State_Alarm
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.alarmBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.alarmPg }
        }
    ]

    transitions: [
        Transition {
            ColorAnimation {property: "bgColor" ; duration: potatoGpu ? 0 : 500}
            ColorAnimation {property: "pgColor" ; duration: potatoGpu ? 0 : 500}
        }
    ]

    Item {
        id: statusDisplay
        anchors.fill: parent

        readonly property string    unitString: (grblStatus.isRawPositionInInches ? qsTrId("label-unit-in") : qsTrId("label-unit-mm"))
        readonly property string    posString:  (grblStatus.isRawPositionInWorkCoord ? qsTrId("label-coordinates-work") : qsTrId("label-coordinates-machine"))
        readonly property vector3d  position:   grblStatus.rawPosition

        TextWithCaption {
            id:grblStateText

            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: parent.width * 0.75
            width: parent.width * 0.25

            valueText.text: textFromState(grblStatus.currentState)
            valueText.color: Style.topbar.value.color

            captionText.text: qsTrId("label-status-title")
            captionText.color: Style.topbar.caption.color

            function textFromState( state )
            {
                switch(state){
                case GrblStatus.State_Idle:
                    return qsTrId("label-grbl-status-idle");
                case GrblStatus.State_Run:
                    return qsTrId("label-grbl-status-running");
                case GrblStatus.State_Hold:
                    return qsTrId("label-grbl-status-paused ");
                case GrblStatus.State_Jog:
                    return qsTrId("label-grbl-status-jogging");
                case GrblStatus.State_Door:
                    return qsTrId("label-grbl-status-door");
                case GrblStatus.State_Home:
                    return qsTrId("label-grbl-status-homing");
                case GrblStatus.State_Alarm:
                    return qsTrId("label-grbl-status-alarm");
                case GrblStatus.State_Check:
                    return qsTrId("label-grbl-status-simulation");
                case GrblStatus.State_Sleep:
                    return qsTrId("label-grbl-status-sleeping")
                case GrblStatus.State_Unknown:
                    return qsTrId("label-grbl-status-unknown");
                case GrblStatus.State_Offline:
                    return qsTrId("label-grbl-status-disconnected");
                }
            }
        }

        Rectangle {
            id:separator1
            width: 1
            color: Style.separator.color
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * Style.separator.span

            x: parent.width * 0.75
        }

        TextWithCaption {
            id:zCoordText

            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: parent.width * 0.5
            width: parent.width * 0.25

            valueText.text: parent.position.z.toFixed(3)
            valueText.color:  Style.topbar.value.color
            captionText.text: parent.posString + "Z" + parent.unitString
            captionText.color:  Style.topbar.caption.color
        }

        Rectangle {
            id:separator2
            width: 1
            color: Style.separator.color
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * Style.separator.span

            x: parent.width * 0.5
        }

        TextWithCaption {
            id:yCoordText

            anchors.right: separator2.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: parent.width * 0.25
            width: parent.width * 0.25

            valueText.text: parent.position.y.toFixed(3)
            valueText.color: Style.topbar.value.color
            captionText.text: parent.posString + "Y" + parent.unitString
            captionText.color: Style.topbar.caption.color
        }

        Rectangle {
            id:separator3
            width: 1
            color: Style.separator.color
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * Style.separator.span

            x: parent.width * 0.25
        }

        TextWithCaption {
            id:xCoordText

            anchors.right: separator3.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: 0
            width: parent.width * 0.25

            valueText.text: parent.position.x.toFixed(3)
            valueText.color: Style.topbar.value.color
            captionText.text: parent.posString + "X" + parent.unitString
            captionText.color: Style.topbar.caption.color

        }
    }
}

#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture;

uniform bool textured;

varying vec4 v_colorinfo;

uniform vec4 color_a;
uniform vec4 color_b;

void main()
{
    vec4 color = textured
            ? texture2D(texture, vec2(v_colorinfo.x, v_colorinfo.y))
            : v_colorinfo;
    gl_FragColor = color * color_a + color_b;
}



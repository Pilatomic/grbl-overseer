#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

//Rendering matrices
uniform mat4 m_matrix;
uniform mat4 vp_matrix;

//Vertex position & color
attribute vec4 a_position;
attribute vec4 a_colorinfo;

//Texture coord
varying vec4 v_colorinfo;

void main()
{
    // Calculate vertex position in screen space
    gl_Position =  vp_matrix * m_matrix * a_position;

    // Pass texture coordinate OR color to fragment shader
    // Value will be automatically interpolated to fragments inside polygon faces
    v_colorinfo = a_colorinfo;
}

